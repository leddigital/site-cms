<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Contents;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Categories();
    }

    public function index(Request $request)
    {
        $_type = $request->route('_type');
        
        return view(
            'admin.categories.index', ['_type' => $_type]
        );
    }

    //Listar todas as categorias
    public function readAll(Request $request)
    {        
        $type = $request->route('_type');

        $collection = $this->model->where('lang', '=', $type)->get()->all();
        
        $data['data'] = $collection;
        echo json_encode($data);
    }


    public function form(Request $request)
    {
        $id = $request->route('id');
        $_type = $request->route('_type'); 

        if (isset($id) and ($id != "")) {
            $entity = $this->model->find($id);
            return view('admin.categories.form', ['entity' => $entity, '_type' => $_type]);
        } else {
            return view('admin.categories.form', ['_type' => $_type]);
        }
    }

    public function save(Request $request)
    {

        $form = $request->all();
        $id = $request->route('id');
        $_type = $request->route('_type');
        $destination_path = public_path() . '/categories';

        if(!isset($destination_path)){
            mkdir($destination_path, 0777);
        }

        if(isset($id) && $id != "") {

            $entity = $this->model->find($id);

            if(isset($form['base64']) && !empty($form['base64'])){
                $form['image'] = $this->saveImg($form['base64'], 'categoria_', '/img/categorias/', $entity->image);
            }
                
            $form['url'] = $this->url_verify($form['title'], $this->model, $id);
            $form['lang'] = $_type;

            $entity->update($form);

            if($entity) {
                $res = [
                    'status' => 200,
                    'data' => $entity
                ];
            } else {
                $res = [
                    'status' => 500,
                    'data' => $entity
                ];
            }

        }
        else {

            $form['image'] = $this->saveImg($form['base64'], 'categoria_' ,'/img/categorias/');
            $form['lang'] = $_type;
            $form['url'] = $this->url_verify($form['title'], $this->model);

            $entity = $this->model->create($form);

            if($entity) {
                $res = [
                    'status' => 200,
                    'data' => $entity
                ];
            } else {
                $res = [
                    'status' => 500,
                    'data' => $entity
                ];
            }
        }
        return response()->json($res);
    }

    public function delete(Request $request){

        $destination = public_path() . '/img/categorias/';
        $id = $request->route('id');
        
        $entity = $this->model->find($id);

        @unlink($destination . $entity->image);
        $entity->delete();
    }

}
