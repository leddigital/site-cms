<?php

namespace App\Http\Controllers;

use App;
use App\Banners;
use App\Categories;
use App\Contents;
use App\Informations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class NavigationController extends Controller
{

    public function __construct()
    {
        // Pega a última imagem de banner inserida para a meta tag de img
        $banners = Banners::get();
        $single_image = $banners->last();

        $informations = Informations::get()->first();
        View::share('informations', $informations);
        View::share('single_image', $single_image);
    }

    public function index(Request $request)
    {
        $banners = Banners::get();
        $categories = Categories::get();

        return view('pages.index', [
            'banners' => $banners, 
            'categories' => $categories
        ]);
    }

    public function sobre(Request $request)
    {
        $_subtitle = 'Conviértete en un Distribuidor';
        return view('pages.sobre', ['_subtitle' => $_subtitle]);
    }

    public function produtos(Request $request)
    {
        $url = $request->route('categoria');
        $categoria = Categories::where('url', '=', $url)->get()->first();
        $produtos = $categoria->contents()->paginate(12);

        $_subtitle = $categoria->title;

        return view('pages.produtos', [
            'categoria' => $categoria, 
            'produtos' => $produtos, 
            '_subtitle' => $_subtitle
        ]);
    }

    public function produto(Request $request)
    {
        $url = $request->route('url');
        $produto = Contents::where('url', '=', $url)->get()->first();
        $categoria = $produto->categories()->get()->first();

        $_subtitle = $produto->title;

        return view('pages.produto', [
            'categoria' => $categoria, 
            'produto' => $produto,
            '_subtitle' => $_subtitle
        ]);
    }

    public function contato(Request $request)
    {
        return view('pages.contato');
    }

    public function procurar(Request $request){

        $termo = $request->query('s');
        $produtos = Contents::where('title', 'like', '%'. $termo .'%')
        ->orWhere('sku', '=', $termo)
        ->orWhere('ncm', '=', $termo)
        ->paginate(12);

        return view('pages.produtos', [
            'termo' => $termo, 
            'produtos' => $produtos 
        ]);
    }

}
