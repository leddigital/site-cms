<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Contents;
use App\ContentsImages;
use Illuminate\Http\Request;

class ContentsController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Contents();
    }

    public function index(Request $request) {
        //Pega o tipo de conteudo que vou gerenciar
        $_type = $request->route('_type');
        return view('admin.content.' . $_type . '.index', ['_type' => $_type]);
    }

    //Listar todas as categorias
    public function readAll(Request $request) {
        $type = $request->route('_type');

        $collection = $this->model->where('lang', '=', $type)->get()->all();
        
        $data['data'] = $collection;
        echo json_encode($data);
    }

    public function form(Request $request) {

        $id = $request->route('id');
        $_type = $request->route('_type');

        if (isset($id) and ($id != "")) {

            $entity = $this->model->with(['categories'])->find($id);
            $gallery = $entity->images()->get();

            $categories = Categories::with(['contents' => function ($q) use ($id) {
                $q->where('id', $id);
            }])->orderBy('title', 'ASC')
                ->get();

            return view('admin.content.' . $_type . '.form', [
                'entity' => $entity,
                'categories' => $categories,
                'gallery' => $gallery,
                '_type' => $_type,
            ]);

        } else {
            $categories = Categories::where('lang', $_type)->orderBy('title', 'ASC')->get();
            return view('admin.content.' . $_type . '.form', ['categories' => $categories, '_type' => $_type]);
        }
    }

    public function save(Request $request) {

        $folder = public_path() . '/img/';
        $form = $request->all();
        $id = $request->route('id');
        $_type = $request->route('_type');


        // Se o tipo for pallet, zerar os valores
        if(isset($form['container']) && strcmp($form['container'], "PALLET") == 0) {
            $form['qtd_caixa'] ="";
            $form['cla'] ="";
            $form['m3_caixa'] ="";
            $form['peso_caixa'] ="";
        }

        // Inserir novo registro
        if (!isset($id) and $id == "") {

            // Salva a imagem
            $form['image'] = $this->saveImg($form['base64'], 'produto_', '/img/produtos/');
            $form['lang'] = $_type;

            $form['url'] = $this->url_verify($form['title'], $this->model);

            // Registra produto no banco de dados
            $entity = $this->model->create($form);

            // Cria as categorias do produto
            $category = new Categories();
  
            if (isset($form['categories']) > 0){
                foreach ($form['categories'] as $category) {
                    $entity->categories()->attach($category);
                }
            }

            if ($entity) {

                $res = [
                    'status' => 200,
                    'data' => $entity,
                ];

            } else {
                $res = [
                    'status' => 500,
                    'data' => $entity,
                ];
            }

        } else {

            //Atualizar o registro
            $entity = $this->model->find($id);

            //Gera a url amigável
            $form['url'] = $this->url_verify($form['title'], $this->model, $id);

            if ($entity->update($form)) {
                if(isset($form['base64']) && $form['base64']!="") {
                    $update['image'] = $this->saveImg($form['base64'], 'produto_', '/img/produtos/', $entity->image);
                    $entity->update($update);
                }
            }

            if (isset($form['categories']) > 0) {
                //Remove todas as categorias e adiciona novamente
                $entity->categories()->detach();
                //Adiciona novamente as categorias
                foreach ($form['categories'] as $category) {
                    $entity->categories()->attach($category);
                }
            }

            $res = [
                'status' => 200,
                'data' => $entity,
            ];
        }

        return response()->json($res);
    }

    public function delete(Request $request) {

        $id = $request->route('id');
        $entity = $this->model->find($id);

        // Excluir categorias
        $categories = $entity->categories()->detach();

        if ($entity->delete()) {
            @unlink(public_path() . '/img/produtos/' . $entity->image);
        }
    }
}
