<?php

namespace App\Http\Controllers;

use App\Contents;
use App\Categories;
use Illuminate\Http\Request;

class InsertProducts extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Contents();
    }

    public function save () {

        $handle = fopen('produtos_es.csv', 'r');
        $header = true;

        $cont = 1;

        while ($data = fgetcsv($handle, 10000, ";")) {

            if ($header) $header = false;
            else {
                $produto = [

                    'title' => $data[0],
                    'sku' => $data[1],
                    'url' => $this->url_verify($data[0], $this->model),
                    'image' => $data[3],
                    'type' => 'product',
                    'validade' => $data[4],
                    'peso_unitario' => $data[5],
                    'ncm' => $data[6],
                    'qtd_caixa' => $data[7],
                    'cla' => $data[8],
                    'm3_caixa' => $data[9],
                    'peso_caixa' => $data[10],
                    'container' => $data[11],
                    'lang' => $data[12],
                ];

                $category = new Categories();
                $entity = $this->model->create($produto);

                $entity->categories()->attach($data[2]);

                if(!file_exists(public_path() . "/img/produtos/" . $produto['image'])) {
                    echo "<p>Imagem " . $produto['image'] . " não existe</p><br>";
                }

                if ($entity) echo $cont . " - <strong>" . $produto['title'] . "</strong> adicionado com sucesso! <br>";
                else echo $cont . " - Falha ao adicionar <strong>". $produto['title'] . "</strong><br>";

                $cont++;
            }

        }

    }
}
