<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Informations extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'address',
        'number',
        'district',
        'zipcode',
        'city',
        'state',
        'whatsapp',
        'instagram',
        'facebook',
        'linkedin',
        'twitter',
        'pinterest',
        'phone1',
        'email',
        'phone2',
        'meta_description'
    ];
}
