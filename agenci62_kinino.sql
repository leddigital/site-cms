-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 02/03/2020 às 09:26
-- Versão do servidor: 5.6.41-84.1
-- Versão do PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `agenci62_kinino`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `lang` varchar(45) DEFAULT 'NULL'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `banners`
--

INSERT INTO `banners` (`id`, `image`, `title`, `created_at`, `updated_at`, `status`, `lang`) VALUES
(21, 'banner_5de92941dda14.jpeg', 'Banner 3', '2019-12-05 15:58:57', '2019-12-05 15:58:57', NULL, 'en'),
(22, 'banner_5de9582e0af52.jpeg', 'Banner 2', '2019-12-05 19:19:10', '2019-12-05 19:19:10', NULL, 'en');

-- --------------------------------------------------------

--
-- Estrutura para tabela `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `short_description` text,
  `image` text,
  `lang` varchar(45) NOT NULL DEFAULT 'NULL'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `categories`
--

INSERT INTO `categories` (`id`, `title`, `url`, `short_description`, `image`, `lang`) VALUES
(1, 'Batatas e Queijos', 'batatas-e-queijos', 'Traga mais sabor para sua empresa', 'batatas-e-queijos.jpg', 'br'),
(2, 'Chás', 'chas', 'Traga mais sabor para sua empresa', 'chas.jpg', 'br'),
(3, 'Chocolates e Doces', 'chocolates-e-doces', 'Traga mais sabor para sua empresa', 'chocolates-e-doces.jpg', 'br'),
(4, 'Condimentos e Especiarias', 'condimentos-e-especiarias', '', 'condimentos-e-especiarias.jpg', 'br'),
(5, 'Farináceos', 'farinaceos', '', 'farinaceos.jpg', 'br'),
(6, 'Gelatinas', 'gelatinas', '', 'gelatinas.jpg', 'br'),
(7, 'Grãos e Leguminosos', 'graos-e-leguminosos', '', 'graos-e-leguminosos.jpg', 'br'),
(8, 'Ketchup, Mostarda, Maionese', 'ketchup-mostarda-maionese', '', 'ketchup-mostarda-maionese.jpg', 'br'),
(9, 'K-Life', 'k-life', '', 'k-life.jpg', 'br'),
(10, 'Molhos Especiais', 'molhos-especiais', '', 'molhos-especiais.jpg', 'br'),
(11, 'Pássaros', 'passaros', '', 'passaros.jpg', 'br'),
(12, 'Pimenta in Natura', 'pimenta-in-natura', '', 'pimenta-in-natura.jpg', 'br'),
(13, 'Pipocas Micro-Ondas', 'pipocas-micro-ondas', '', 'pipocas-microondas.jpg', 'br'),
(14, 'Prático Sabor', 'pratico-sabor', '', 'pratico-sabor.jpg', 'br'),
(15, 'Temperos', 'temperos', '', 'temperos.jpg', 'br'),
(16, 'Temperos Especiais', 'temperos-especiais', '', 'temperos-especiais.jpg', 'br');

-- --------------------------------------------------------

--
-- Estrutura para tabela `categories_has_contents`
--

CREATE TABLE `categories_has_contents` (
  `categories_id` int(11) NOT NULL,
  `contents_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `categories_has_contents`
--

INSERT INTO `categories_has_contents` (`categories_id`, `contents_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(2, 6),
(2, 7),
(2, 8),
(2, 9),
(2, 10),
(2, 11),
(2, 12),
(2, 13),
(2, 14),
(2, 15),
(2, 16),
(2, 17),
(2, 18),
(2, 19),
(3, 20),
(3, 21),
(3, 22),
(3, 23),
(3, 24),
(3, 25),
(3, 26),
(3, 27),
(3, 28),
(3, 29),
(3, 30),
(3, 31),
(3, 32),
(3, 33),
(3, 34),
(3, 35),
(3, 36),
(3, 37),
(3, 38),
(3, 39),
(3, 40),
(3, 41),
(3, 42),
(4, 43),
(4, 44),
(4, 45),
(4, 46),
(4, 47),
(4, 48),
(4, 49),
(4, 50),
(4, 51),
(4, 52),
(4, 53),
(4, 54),
(4, 55),
(4, 56),
(4, 57),
(4, 58),
(4, 59),
(4, 60),
(4, 61),
(4, 62),
(4, 63),
(4, 64),
(4, 65),
(4, 66),
(4, 67),
(4, 68),
(4, 69),
(4, 70),
(4, 71),
(5, 72),
(5, 73),
(5, 74),
(5, 75),
(5, 76),
(5, 77),
(5, 78),
(5, 79),
(5, 80),
(5, 81),
(5, 82),
(5, 83),
(5, 84),
(5, 85),
(5, 86),
(5, 87),
(5, 88),
(5, 89),
(5, 90),
(5, 91),
(5, 92),
(5, 93),
(5, 94),
(5, 95),
(5, 96),
(5, 97),
(5, 98),
(5, 99),
(5, 100),
(5, 101),
(5, 102),
(6, 103),
(6, 104),
(6, 105),
(6, 106),
(6, 107),
(6, 108),
(6, 109),
(6, 110),
(6, 111),
(6, 112),
(6, 113),
(6, 114),
(6, 115),
(6, 116),
(6, 117),
(6, 118),
(6, 119),
(6, 120),
(7, 121),
(7, 122),
(7, 123),
(7, 124),
(7, 125),
(7, 126),
(7, 127),
(9, 128),
(9, 129),
(9, 130),
(9, 131),
(9, 132),
(9, 133),
(9, 134),
(9, 135),
(9, 136),
(9, 137),
(9, 138),
(9, 139),
(9, 140),
(9, 141),
(9, 142),
(9, 143),
(9, 144),
(9, 145),
(9, 146),
(9, 147),
(9, 148),
(9, 149),
(9, 150),
(9, 151),
(9, 152),
(9, 153),
(9, 154),
(9, 155),
(9, 156),
(9, 157),
(9, 158),
(8, 159),
(8, 160),
(8, 161),
(8, 162),
(10, 163),
(10, 164),
(10, 165),
(10, 166),
(10, 167),
(10, 168),
(10, 169),
(10, 170),
(10, 171),
(10, 172),
(10, 173),
(10, 174),
(10, 175),
(10, 176),
(10, 177),
(10, 178),
(10, 179),
(10, 180),
(10, 181),
(10, 182),
(10, 183),
(10, 184),
(10, 185),
(10, 186),
(11, 187),
(11, 188),
(11, 189),
(11, 190),
(12, 191),
(12, 192),
(12, 193),
(12, 194),
(12, 195),
(12, 196),
(12, 197),
(12, 198),
(12, 199),
(13, 200),
(13, 201),
(13, 202),
(13, 203),
(13, 204),
(13, 205),
(15, 206),
(15, 207),
(15, 208),
(15, 209),
(15, 210),
(15, 211),
(15, 212),
(15, 213),
(15, 214),
(15, 215),
(15, 216),
(15, 217),
(15, 218),
(15, 219),
(15, 220),
(15, 221),
(15, 222),
(15, 223),
(15, 224),
(15, 225),
(15, 226),
(15, 227),
(15, 228),
(15, 229),
(15, 230),
(15, 231),
(15, 232),
(15, 233),
(15, 234),
(15, 235),
(15, 236),
(15, 237),
(15, 238),
(15, 239),
(15, 240),
(15, 241),
(15, 242),
(15, 243),
(15, 244),
(16, 245),
(16, 246),
(16, 247),
(16, 248),
(16, 249),
(16, 250),
(16, 251),
(16, 252),
(16, 253),
(16, 254),
(16, 255),
(16, 256),
(16, 257),
(14, 258),
(14, 259),
(14, 260),
(14, 261),
(14, 262),
(14, 263),
(14, 264),
(14, 265),
(14, 266),
(5, 267),
(9, 268),
(16, 269),
(16, 270),
(16, 271),
(15, 272),
(15, 273),
(15, 274),
(15, 275),
(16, 276),
(16, 277),
(4, 278),
(4, 279),
(4, 280),
(4, 281),
(4, 282),
(4, 283),
(4, 284),
(4, 285),
(4, 286),
(4, 287),
(4, 288),
(1, 289);

-- --------------------------------------------------------

--
-- Estrutura para tabela `contents`
--

CREATE TABLE `contents` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `sku` int(11) NOT NULL DEFAULT '0',
  `type` varchar(45) NOT NULL COMMENT 'post,news,page,page-name',
  `url` varchar(255) NOT NULL,
  `conjunto` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) DEFAULT NULL,
  `validade` varchar(45) NOT NULL DEFAULT '0',
  `peso_unitario` varchar(45) NOT NULL DEFAULT '0',
  `ncm` varchar(45) NOT NULL DEFAULT '0',
  `qtd_caixa` varchar(45) NOT NULL DEFAULT '0',
  `cla` varchar(45) NOT NULL DEFAULT '0',
  `m3_caixa` varchar(45) NOT NULL DEFAULT '0',
  `peso_caixa` varchar(45) NOT NULL DEFAULT '0',
  `container` varchar(45) NOT NULL DEFAULT '0',
  `lang` varchar(45) NOT NULL DEFAULT 'NULL',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `contents`
--

INSERT INTO `contents` (`id`, `title`, `sku`, `type`, `url`, `conjunto`, `image`, `validade`, `peso_unitario`, `ncm`, `qtd_caixa`, `cla`, `m3_caixa`, `peso_caixa`, `container`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'BATATA PALHA TRADICIONAL', 273, 'product', 'batata-palha-tradicional', '', 'cod-273.jpg', '6', '120G', '20052000', '26X120G', '42,0 X 26,0 X 26,5', '0,0289', '3,48', 'CAIXA', 'br', '2019-12-04 19:57:07', '2019-12-04 19:57:07'),
(2, 'BATATA PALHA TRADICIONAL', 274, 'product', 'batata-palha-tradicional-1', '', 'cod-274.jpg', '6', '250G', '20052000', '12X250G', '31,0 X 20,5 X 16,5', '0,0105', '3,32', 'CAIXA', 'br', '2019-12-04 19:57:07', '2019-12-04 19:57:07'),
(3, 'BATATA ONDULADA TRADICIONAL', 275, 'product', 'batata-ondulada-tradicional', '', 'cod-275.jpg', '6', '40G', '20052000', '20X40G', '56,0 X 30,0 X 14,5', '0,0244', '1,1', 'CAIXA', 'br', '2019-12-04 19:57:07', '2019-12-04 19:57:07'),
(4, 'PURE DE BATATA', 280, 'product', 'pure-de-batata', '', 'cod-280.jpg', '12', '160G', '11052000', '24X160G', '31,0 X 20,5 X 16,5', '0,0105', '2,08', 'CAIXA', 'br', '2019-12-04 19:57:07', '2019-12-04 19:57:07'),
(5, 'QUEIJO PARMESÃO RALADO', 335, 'product', 'queijo-parmesao-ralado', '', 'cod-335.jpg', '6', '40G', '4062000', '100X40G', '42,5 X 32,5 X 21,0', '0,029', '5,81', 'CAIXA', 'br', '2019-12-04 19:57:07', '2019-12-04 19:57:07'),
(6, 'CHÁ BRANCO SACHÊ', 238, 'product', 'cha-branco-sache', '', 'cod-238.jpg', '24', '15G', '9022000', '24X15G', '26,0 X 15,0 X 14,0', '0,0055', '0,63', 'CAIXA', 'br', '2019-12-04 19:57:07', '2019-12-04 19:57:07'),
(7, 'CHÁ DE CAMOMILA SACHÊ', 191, 'product', 'cha-de-camomila-sache', '', 'cod-191.jpg', '24', '10G', '9022000', '24X10G', '26,0 X 15,0 X 14,0', '0,0055', '0,51', 'CAIXA', 'br', '2019-12-04 19:57:07', '2019-12-04 19:57:07'),
(8, 'CHÁ DE CAPIM-CIDREIRA SACHÊ', 194, 'product', 'cha-de-capim-cidreira-sache', '', 'cod-194.jpg', '24', '10G', '9022000', '24X10G', '26,0 X 15,0 X 14,0', '0,0055', '0,51', 'CAIXA', 'br', '2019-12-04 19:57:07', '2019-12-04 19:57:07'),
(9, 'CHÁ DE ERVA DOCE NACIONAL SACHÊ', 193, 'product', 'cha-de-erva-doce-nacional-sache', '', 'cod-193.jpg', '24', '15G', '9096210', '24X15G', '26,0 X 15,0 X 14,0', '0,0055', '0,63', 'CAIXA', 'br', '2019-12-04 19:57:07', '2019-12-04 19:57:07'),
(10, 'CHÁ DE FRUTAS VERMELHAS SACHÊ', 301, 'product', 'cha-de-frutas-vermelhas-sache', '', 'cod-301.jpg', '24', '20G', '9022000', '24X20G', '26,0 X 15,0 X 14,0', '0,0055', '0,75', 'CAIXA', 'br', '2019-12-04 19:57:07', '2019-12-04 19:57:07'),
(11, 'CHÁ DE HIBISCO SACHÊ', 300, 'product', 'cha-de-hibisco-sache', '', 'cod-300.jpg', '24', '10G', '9022000', '24X10G', '26,0 X 15,0 X 14,0', '0,0055', '0,51', 'CAIXA', 'br', '2019-12-04 19:57:07', '2019-12-04 19:57:07'),
(12, 'CHÁ DE HORTELÃ SACHÊ', 190, 'product', 'cha-de-hortela-sache', '', 'cod-190.jpg', '24', '10G', '9022000', '24X10G', '26,0 X 15,0 X 14,0', '0,0055', '0,51', 'CAIXA', 'br', '2019-12-04 19:57:07', '2019-12-04 19:57:07'),
(13, 'CHA DE MAÇA COM CANELA', 192, 'product', 'cha-de-maca-com-canela', '', 'cod-192.jpg', '24', '20G', '9022000', '24X20G', '26,0 X 15,0 X 14,0', '0,0055', '0,75', 'CAIXA', 'br', '2019-12-04 19:57:07', '2019-12-04 19:57:07'),
(14, 'CHÁ VERDE SACHÊ', 236, 'product', 'cha-verde-sache', '', 'cod-236.jpg', '24', '15G', '9022000', '24X15G', '26,0 X 15,0 X 14,0', '0,0055', '0,63', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(15, 'CHÁ VERDE', 172, 'product', 'cha-verde', '', 'cod-172.jpg', '24', '80G', '9021010', '24X80G', '40,5 X 18,5 X 30,0', '0,0225', '2,45', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(16, 'CHA MATE TOSTADO SACHÊ', 232, 'product', 'cha-mate-tostado-sache', '', 'cod-232.jpg', '24', '40G', '9030010', '24X40G', '36,5 X 31,0 X 31,0', '0,0351', '8,73', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(17, 'CHÁ MATE TOSTADO', 265, 'product', 'cha-mate-tostado', '', 'cod-265.jpg', '24', '100G', '9030010', '24X100G', '26,0 X 25,5 X 25,5', '0,0169', '3,43', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(18, 'CHÁ MATE TOSTADO', 234, 'product', 'cha-mate-tostado-1', '', 'cod-234.jpg', '24', '250G', '9030010', '20X250G', '36,5 X 31,0 X 31,0', '0,0351', '8,73', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(19, 'CHÁ PRETO SACHÊ', 272, 'product', 'cha-preto-sache', '', 'cod-272.jpg', '24', '18G', '9023000', '24X18G', '26,0 X 15,0 X 14,0', '0,0055', '0,7', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(20, 'ACHOCOLATADO INSTANTÂNEO PLUS', 374, 'product', 'achocolatado-instantaneo-plus', '', 'cod-374.jpg', '12', '200G', '18069000', '24X200G', '32,0 X 21,0 X 16,0', '0,0108', '4,97', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(21, 'ACHOCOLATADO INSTANTÂNEO PLUS', 129, 'product', 'achocolatado-instantaneo-plus-1', '', 'cod-129.jpg', '12', '400G', '18069000', '24X400G', '45,0 X 32,0 X 15,0', '0,0216', '10,43', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(22, 'ACHOCOLATADO INSTANTÂNEO PLUS', 128, 'product', 'achocolatado-instantaneo-plus-2', '', 'cod-128.jpg', '12', '1,01KG', '18069000', '12X1,01KG', '45,0 X 19,0 X 42,0', '0,0359', '12,72', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(23, 'ACHOCOLATADO INSTANTÂNEO ANIÁ', 75, 'product', 'achocolatado-instantaneo-ania', '', 'cod-75.jpg', '12', '400G', '18069000', '24X400G', '33,0 X 40,0 X 8,0', '0,0106', '9,98', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(24, 'AMIDO DE MILHO', 372, 'product', 'amido-de-milho', '', 'cod-372.jpg', '24', '200G', '11081200', '24X200G', '32,0 X 27,0 X 12,0', '0,0104', '4,97', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(25, 'AMIDO DE MILHO', 137, 'product', 'amido-de-milho-1', '', 'cod-137.jpg', '24', '500G', '11081200', '24X500G', '37,0 X 44,0 X 13,0', '0,0212', '12,44', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(26, 'AMIDO DE MILHO', 195, 'product', 'amido-de-milho-2', '', 'cod-195.jpg', '24', '1KG', '11081200', '12X1KG', '37,0 X 49,0 X 13,0', '0,0236', '12,22', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(27, 'AMIDO DE MILHO - CAIXA', 145, 'product', 'amido-de-milho-caixa', '', 'cod-145.jpg', '24', '500G', '11081200', '24X500G', '25,0 X 27,0 X 20,0', '0,0135', '6,65', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(28, 'AMIDO DE MILHO - CAIXA', 264, 'product', 'amido-de-milho-caixa-1', '', 'cod-264.jpg', '24', '200G', '11081200', '24X200G', '26,5 X 21,0 X 27,0', '0,015', '6,02', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(29, 'AROMA ABACAXI', 121, 'product', 'aroma-abacaxi', '', 'cod-121.jpg', '36', '30G', '33021000', '12X30ML', '6,5 X 19,5 X 8,0', '0,001', '0,5', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(30, 'AROMA DE BAUNILHA', 120, 'product', 'aroma-de-baunilha', '', 'cod-120.jpg', '36', '30G', '33021000', '12X30ML', '6,5 X 19,5 X 8,0', '0,001', '0,5', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(31, 'AROMA DE COCO', 122, 'product', 'aroma-de-coco', '', 'cod-122.jpg', '36', '30G', '33021000', '12X30ML', '6,5 X 19,5 X 8,0', '0,001', '0,5', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(32, 'CHOCOLATE EM PÓ SOLÚVEL 40%', 119, 'product', 'chocolate-em-po-soluvel-40%', '', 'cod-119.jpg', '12', '200G', '18061000', '12X200G', '11,0 X 16,5 X 17,0', '0,0031', '3,01', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(33, 'CONFEITO GRANULADO CHOCOLATE', 15, 'product', 'confeito-granulado-chocolate', '', 'cod-15.jpg', '12', '120G', '18063220', '40X120G', '28,5 X 29,0 X 14,0', '0,0116', '5,74', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(34, 'CONFEITO GRANULADO CHOCOLATE', 92, 'product', 'confeito-granulado-chocolate-1', '', 'cod-92.jpg', '12', '70G', '18063220', '24X70G', '21,5 X 17,5 X 16,0  ', '0,006', '1,95', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(35, 'CONFEITO GRANULADO CHOCOLATE', 130, 'product', 'confeito-granulado-chocolate-2', '', 'cod-130.jpg', '12', '500G', '18063220', '12X500G', '28,0 X 29,0 X 14,0', '0,0114', '6,38', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(36, 'COCO RALADO ÚNIDO E ADOÇADO', 333, 'product', 'coco-ralado-unido-e-adocado', '', 'cod-333.jpg', '12', '100G', '8011100', '24X100G', '31,0 X 23,0 X 13,5', '0,0096', '2,52', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(37, 'LEITE DE COCO', 334, 'product', 'leite-de-coco', '', 'cod-334.jpg', '24', '328G', '20098990', '24X200ML', '35,0 x 23,0 x 13,5', '0,0109', '7,8', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(38, 'FERMENTO BIOLÓGICO SECO', 276, 'product', 'fermento-biologico-seco', '', 'cod-276.jpg', '12', '10G', '21021090', '120X10G', '20,0 X 9,0 X 10,0', '0,0018', '0,36', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(39, 'FERMENTO EM PÓ QUÍMICO', 146, 'product', 'fermento-em-po-quimico', '', 'cod-146.jpg', '12', '100G', '21023000', '72X100G', '49,0 X 22,0 X 15,5', '0,0167', '9,34', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(40, 'MISTURA PARA SAGU SABOR MORANGO', 303, 'product', 'mistura-para-sagu-sabor-morango', '', 'cod-303.jpg', '12', '250G', '11062000', '12X250G', '26,5 X 23,0 X 18,0', '0,011', '2,43', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(41, 'MISTURA PARA SAGU SABOR UVA', 302, 'product', 'mistura-para-sagu-sabor-uva', '', 'cod-302.jpg', '12', '250G', '11062000', '12X250G', '26,5 X 23,0 X 18,0', '0,011', '3,43', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(42, 'SAGU', 64, 'product', 'sagu', '', 'cod-64.jpg', '12', '500G', '11062000', '12X500G', '33,0 X 45,0 X 12,0', '0,0178', '12,42', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(43, 'SAL AMONÍACO', 40, 'product', 'sal-amoniaco', '', 'cod-40.jpg', '12', '50G', '28369913', '24X50G', '21,5 X 17,5 X 16,0', '0,006', '2,11', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(44, 'BICARBONATO DE SÓDIO', 22, 'product', 'bicarbonato-de-sodio', '', 'cod-22.jpg', '24', '70G', '28363000', '24X70G', '21,5 X 17,5 X 16,0', '0,006', '1,9', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(45, 'BICARBONATO DE SÓDIO', 351, 'product', 'bicarbonato-de-sodio-1', '', 'cod-351.jpg', '24', '500G', '28363000', '24X500G', '34,0 X 36,0 X 12,0', '0,0147', '12,24', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(46, 'CAMOMILA FLOR', 196, 'product', 'camomila-flor', '', 'cod-196.jpg', '12', '5G', '12119090', '24X5G', '20,0 X 13,0 X 8,0', '0,0021', '0,29', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(47, 'CEBOLINHA DESIDRATADA', 38, 'product', 'cebolinha-desidratada', '', 'cod-38.jpg', '12', '10G', '7129090', '24X10G', '18,0 X 12,0 X 9,0', '0,0019', '0,42', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(48, 'CHEIRO VERDE', 57, 'product', 'cheiro-verde', '', 'cod-57.jpg', '12', '7G', '7129090', '24X7G', '21,5 X 15,5 X 8,0', '0,0027', '3,53', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(49, 'COLORÍFICO', 95, 'product', 'colorifico', '', 'cod-95.jpg', '12', '70G', '17035000', '100X70G', '32,0 X 29,0 X 18,0', '0,0167', '7,68', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(50, 'COLORÍFICO', 373, 'product', 'colorifico-1', '', 'cod-373.jpg', '12', '200G', '21039021', '24X200G', '32,0 X 21,0 X 16,0', '0,0108', '4,97', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(51, 'COLORÍFICO', 68, 'product', 'colorifico-2', '', 'cod-68.jpg', '12', '500G', '21039021', '12X500G', '32,0 X 20,0 X 13,0', '0,0083', '6,24', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(52, 'COLORÍFICO', 240, 'product', 'colorifico-3', '', 'cod-240.jpg', '12', '1,01KG', '21039029', '12X1,01KG', '39,0 X 37,0 X 12,0', '0,0173', '12,43', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(53, 'AÇAFRÃO EM PÓ', 55, 'product', 'acafrao-em-po', '', 'cod-55.jpg', '12', '30G', '9102000', '24X30G', '22,0 X 17,5 X 13,0', '0,005', '0,92', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(54, 'ERVA-DOCE', 41, 'product', 'erva-doce', '', 'cod-41.jpg', '18', '20G', '9096110', '24X20G', '20,0 X 12,0 X 9,0', '0,0022', '0,68', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(55, 'LOURO EM FOLHAS', 28, 'product', 'louro-em-folhas', '', 'cod-28.jpg', '24', '7G', '9109900', '24X7G', '20,0 X 16,0 X 9,0', '0,0029', '0,35', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(56, 'LOURO EM PÓ', 83, 'product', 'louro-em-po', '', 'cod-83.jpg', '12', '10G', '9109900', '24X10G', '19,5 X 5,0 X 9,0', '0,0009', '0,45', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(57, 'MANJERICÃO', 54, 'product', 'manjericao', '', 'cod-54.jpg', '12', '10G', '12119090', '24X10G', '21,5 X 16,0 X 9,0', '0,0031', '0,043', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(58, 'NOZ MOSCADA BOLA', 29, 'product', 'noz-moscada-bola', '', 'cod-29.jpg', '24', '20G', '21041011', '24X2UNID', '19,0 X 9,0 X 9,0', '0,0015', '0,42', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(59, 'NOZ MOSCADA MOÍDA', 126, 'product', 'noz-moscada-moida', '', 'cod-126.jpg', '12', '15G', '9081200', '24X15G', '20,0 X 9,0 X 9,0', '0,0016', '2,26', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(60, 'PÁPRICA DOCE', 399, 'product', 'paprica-doce', '', 'cod-399.jpg', '12', '20G', '9041200', '24X20G', '22,0 X 17,0 X 13,0', '0,0049', '0,67', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(61, 'PÁPRICA PICANTE', 400, 'product', 'paprica-picante', '', 'cod-400.jpg', '12', '20G', '9041200', '24X20G', '22,0 X 17,0 X 13,0', '0,0049', '0,67', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(62, 'PIMENTA-DO-REINO BRANCA MOÍDA', 49, 'product', 'pimenta-do-reino-branca-moida', '', 'cod-49.jpg', '18', '30G', '9041200', '24X30G', '22,0 X 17,5 X 13,0', '0,005', '0,92', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(63, 'PIMENTA CALABRESA', 69, 'product', 'pimenta-calabresa', '', 'cod-69.jpg', '12', '20G', '9042100', '24X20G', '20,0 X 14,0 X 9,0', '0,0025', '0,67', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(64, 'PIMENTA COM COMINHO', 47, 'product', 'pimenta-com-cominho', '', 'cod-47.jpg', '18', '30G', '9041200', '24X30G', '22,0 X 17,5 X 13,0', '0,005', '0,94', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(65, 'COMINHO EM PÓ', 26, 'product', 'cominho-em-po', '', 'cod-26.jpg', '12', '40G', '9093200', '24X40G', '22,0 X 17,5 X 15,5', '0,006', '1,16', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(66, 'PIMENTA-DO-REINO EM GRÃO', 32, 'product', 'pimenta-do-reino-em-grao', '', 'cod-32.jpg', '18', '40G', '9041200', '24X40G', '22,0 X 17,5 X 13,5', '0,0052', '1,21', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(67, 'PIMENTA-DO-REINO MOÍDA', 44, 'product', 'pimenta-do-reino-moida', '', 'cod-44.jpg', '18', '40G', '9041200', '125X40G', '30,0 X 29,5 X 16,0', '0,0142', '5,91', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(68, 'PIMENTA-DO-REINO MOÍDA', 378, 'product', 'pimenta-do-reino-moida-1', '', 'cod-378.jpg', '18', '200G', '9041200', '12X200G', '29,5 X 22,5 X 11,5', '0,0076', '2,48', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(69, 'PIMENTA-DO-REINO MOÍDA ANIÁ', 332, 'product', 'pimenta-do-reino-moida-ania', '', 'cod-332.jpg', '18', '30G', '9041200', '125X30G', '31,0 X 30,0 X 16,0', '0,0149', '4,25', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(70, 'SALSA DESIDRATADA', 50, 'product', 'salsa-desidratada', '', 'cod-50.jpg', '12', '10G', '7129090', '24X10G', '20,0 X 13,0 X 9,0', '0,0023', '0,44', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(71, 'ALECRIM', 402, 'product', 'alecrim', '', 'cod-402.jpg', '12', '20G', '9109900', '24X20G', '19,5 X 5,0 X 9,0', '0,0009', '0,67', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(72, 'CANJICA AMARELA', 45, 'product', 'canjica-amarela', '', 'cod-45.jpg', '12', '500G', '11042300', '24X500G', '37,0 X 36,0 X 12,0', '0,016', '12,18', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(73, 'CANJICA BRANCA', 1, 'product', 'canjica-branca', '', 'cod-1.jpg', '12', '500G', '11042300', '24X500G', '36,0 X 35,0 X 12,0', '0,0151', '12,22', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(74, 'CANJIQUINHA XERÉM', 291, 'product', 'canjiquinha-xerem', '', 'cod-291.jpg', '12', '500G', '12042300', '24X500G', '35,0 X 38,0 X 11,0', '0,0146', '12,26', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(75, 'QUIRERA FINA DE MILHO', 381, 'product', 'quirera-fina-de-milho', '', 'cod-381.jpg', '12', '500G', '11031300', '24X500G', '33,0 X 43,0 X 11,0', '0,0156', '12,23', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(76, 'PIPOCA AMERICANA', 9, 'product', 'pipoca-americana', '', 'cod-9.jpg', '12', '500G', '10059010', '24X500G', '35,0 x 36,0 x 11,0', '0,0139', '12,28', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(77, 'POP CORN IMPORTADA', 10, 'product', 'pop-corn-importada', '', 'cod-10.jpg', '12', '500G', '10059010', '24X500G', '38,0 X 37,0 X 11,0', '0,0155', '12,19', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(78, 'FAROFA PRONTA TRADICIONAL', 147, 'product', 'farofa-pronta-tradicional', '', 'cod-147.jpg', '8', '400G', '19019090', '24X400G', '43,0 X 16,5 X 49,5', '0,0351', '9,98', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(79, 'FAROFA PRONTA PICANTE', 94, 'product', 'farofa-pronta-picante', '', 'cod-94.jpg', '8', '400G', '19019090', '24X400G', '43,0 X 16,5 X 49,5', '0,0351', '9,98', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(80, 'FAROFA PRONTA SABOR BACON', 124, 'product', 'farofa-pronta-sabor-bacon', '', 'cod-124.jpg', '8', '400G', '19019090', '24X400G', '43,0 X 16,5 X 49,0', '0,0348', '9,98', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(81, 'FAROFA DE SOJA CROCANTE', 386, 'product', 'farofa-de-soja-crocante', '', 'cod-386.jpg', '8', '250G', '19019090', '24X250G', '34,0 X 34,0 X 10,0', '0,0116', '6,3', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(82, 'FAROFA PRONTA DE MILHO', 125, 'product', 'farofa-pronta-de-milho', '', 'cod-125.jpg', '8', '400G', '19019090', '24X400G', '35,0 X 15,0 X 38,5', '0,0202', '9,92', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(83, 'FAROFA PRONTA SABOR PICANHA', 279, 'product', 'farofa-pronta-sabor-picanha', '', 'cod-279.jpg', '8', '250G', '19019090', '24X250G', '34,0 X 34,0 X 10,0', '0,0116', '6,32', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(84, 'FARINHA DE MANDIOCA BIJU', 5, 'product', 'farinha-de-mandioca-biju', '', 'cod-5.jpg', '12', '500G', '11062000', '20X500G', '35,0 X 64,0 X 18,0', '0,0403', '10,35', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(85, 'FARINHA DE MILHO', 2, 'product', 'farinha-de-milho', '', 'cod-2.jpg', '12', '500G', '11022000', '20X500G', '37,0 X 71,0 X 13,0', '0,0342', '10,3', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(86, 'TAPIOCA HIDRATADA', 306, 'product', 'tapioca-hidratada', '', 'cod-306.jpg', '6', '400G', '19030000', '24X400G', '44,0 X 29,5 X 17,0', '0,0221', '10,15', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(87, 'FARINHA DE MILHO FLOCÃO', 281, 'product', 'farinha-de-milho-flocao', '', 'cod-281.jpg', '8', '500G', '11022000', '24X500G', '32,0 X 48,0 X 16,0', '0,0246', '12,33', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(88, 'FUBÁ PRÉ-COZIDO', 283, 'product', 'fuba-pre-cozido', '', 'cod-283.jpg', '8', '500G', '11022000', '24X500G', '37,0 X 44,0 X 15,0', '0,0244', '12,24', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(89, 'FUBÁ MIMOSO', 8, 'product', 'fuba-mimoso', '', 'cod-8.jpg', '12', '500G', '11022000', '24X500G', '34,0 X 40,0 X 12,0', '0,0163', '12,23', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(90, 'FUBÁ MIMOSO', 7, 'product', 'fuba-mimoso-1', '', 'cod-7.jpg', '12', '1KG', '11022000', '12X1KG', '24,0 X 40,0 X 17,0', '0,0163', '12,32', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(91, 'FARINHA DE MANDIOCA CRUA', 3, 'product', 'farinha-de-mandioca-crua', '', 'cod-3.jpg', '12', '500G', '11062000', '24X500G', '32,0 X 45,0 X 11,0', '0,0158', '12,31', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(92, 'FARINHA DE MANDIOCA GROSSA', 39, 'product', 'farinha-de-mandioca-grossa', '', 'cod-39.jpg', '12', '1,0KG', '11062000', '15X1KG', '37,0 X 55,0 X 17,0', '0,0346', '15,26', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(93, 'FARINHA DE MANDIOCA TORRADA', 4, 'product', 'farinha-de-mandioca-torrada', '', 'cod-4.jpg', '12', '500G', '11062000', '24X500G', '36,0 x 47,0 x 13,0', '0,022', '12,44', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(94, 'FARINHA DE ROSCA', 6, 'product', 'farinha-de-rosca', '', 'cod-6.jpg', '12', '500G', '19059000', '24X500G', '32,0 X 12,0 X 42,0', '0,0161', '12,26', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(95, 'POLVILHO AZEDO', 12, 'product', 'polvilho-azedo', '', 'cod-12.jpg', '12', '500G', '11081400', '24X500G', '40,0X 40,0 X 11,0', '0,0176', '12,36', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(96, 'POLVILHO DOCE', 11, 'product', 'polvilho-doce', '', 'cod-11.jpg', '12', '500G', '11081400', '24X500G', '31,0 X 34,0 X 14,0', '0,0148', '12,22', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(97, 'TRIGO PARA KIBE', 14, 'product', 'trigo-para-kibe', '', 'cod-14.jpg', '12', '500G', '11031100', '24X500G', '33,0 X 46,0 X 11,0', '0,0167', '12,21', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(98, 'TRIGO PARA KIBE', 56, 'product', 'trigo-para-kibe-1', '', 'cod-56.jpg', '12', '1KG', '11031100', '12X1K', '37,0 X 24,0 X 15,0', '0,0133', '12,2', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(99, 'FARINHA DE ROSCA', 200, 'product', 'farinha-de-rosca-1', '', 'cod-200.jpg', '12', '5,0KG', '19059090', '1X5KG', '37,5 X 42,5 X 2,5', '0,004', '5,05', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(100, 'FARINHA DE MANDIOCA CRUA', 199, 'product', 'farinha-de-mandioca-crua-1', '', 'cod-199.jpg', '12', '5,0KG', '11062000', '1X5KG', '35,0 X 6,0 X 39,0', '0,0082', '5,05', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(101, 'FUBÁ MIMOSO', 198, 'product', 'fuba-mimoso-2', '', 'cod-198.jpg', '8', '5KG', '11022000', '1X5KG', '32,0 X 6,0 X 37,0', '0,0071', '5,05', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(102, 'TRIGO PARA KIBE', 201, 'product', 'trigo-para-kibe-2', '', 'cod-201.jpg', '12', '5KG', '11031100', '1X5KG', '32,0 X 7,0 X 37,0', '0,0083', '5,05', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(103, 'GELATINA SABOR UVA', 204, 'product', 'gelatina-sabor-uva', '', 'cod-204.jpg', '12', '20G', '21069029', '36X20G', '26,0 X 18,0 X 17,0', '0,008', '0,59', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(104, 'GELATINA SABOR ABACAXI', 205, 'product', 'gelatina-sabor-abacaxi', '', 'cod-205.jpg', '12', '20G', '21069029', '36X20G', '26,0 X 18,0 X 17,0', '0,008', '0,59', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(105, 'GELATINA SABOR CEREJA', 207, 'product', 'gelatina-sabor-cereja', '', 'cod-207.jpg', '12', '20G', '21069029', '36X20G', '26,0 X 18,0 X 17,0', '0,008', '0,59', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(106, 'GELATINA SABOR FRAMBOESA', 206, 'product', 'gelatina-sabor-framboesa', '', 'cod-206.jpg', '12', '20G', '21069029', '36X20G', '26,0 X 18,0 X 17,0', '0,008', '0,59', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(107, 'GELATINA SABOR FRUTAS VERMELHAS', 242, 'product', 'gelatina-sabor-frutas-vermelhas', '', 'cod-242.jpg', '12', '20G', '21069029', '36X20G', '26,0 X 18,0 X 17,0', '0,008', '0,59', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(108, 'GELATINA SABOR LIMÃO', 208, 'product', 'gelatina-sabor-limao', '', 'cod-208.jpg', '12', '20G', '21069029', '36X20G', '26,0 X 18,0 X 17,0', '0,008', '0,59', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(109, 'GELATINA SABOR MARACUJÁ', 235, 'product', 'gelatina-sabor-maracuja', '', 'cod-235.jpg', '12', '20G', '21069029', '36X20G', '26,0 X 18,0 X 17,0', '0,008', '0,59', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(110, 'GELATINA SABOR MORANGO', 203, 'product', 'gelatina-sabor-morango', '', 'cod-203.jpg', '12', '20G', '21069029', '36X20G', '26,0 X 18,0 X 17,0', '0,008', '0,59', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(111, 'GELATINA ZERO AÇUCAR ABACAXI', 216, 'product', 'gelatina-zero-acucar-abacaxi', '', 'cod-216.jpg', '12', '12G', '21069029', '36X12G', '26,0 X 18,0 X 17,0', '0,008', '0,59', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(112, 'GELATINA ZERO AÇUCAR LIMÃO', 218, 'product', 'gelatina-zero-acucar-limao', '', 'cod-218.jpg', '12', '12G', '21069029', '36X12G', '26,0 X 18,0 X 17,0', '0,008', '0,59', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(113, 'GELATINA ZERO AÇUCAR MORANGO', 215, 'product', 'gelatina-zero-acucar-morango', '', 'cod-215.jpg', '12', '12G', '21069029', '36X12G', '26,0 X 18,0 X 17,0', '0,008', '0,59', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(114, 'GELATINA ZERO AÇUCAR UVA', 217, 'product', 'gelatina-zero-acucar-uva', '', 'cod-217.jpg', '12', '12G', '21069029', '36X12G', '26,0 X 18,0 X 17,0', '0,008', '0,59', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(115, 'GELATINA SEM SABOR', 350, 'product', 'gelatina-sem-sabor', '', 'cod-350.jpg', '12', '12G', '21069029', '36X12G', '26,0 X 18,0 X 17,0', '0,008', '0,59', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(116, 'CHANTILLY', 256, 'product', 'chantilly', '', 'cod-256.jpg', '12', '50G', '21069021', '18X50G', '26,5 X 16,0 X 21,5', '0,0091', '1,08', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(117, 'MARIA MOLA', 214, 'product', 'maria-mola', '', 'cod-214.jpg', '12', '50G', '21069021', '36X50G', '26,0 X 18,0 X 17,0', '0,008', '2,32', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(118, 'PUDIM SABOR BAUNILHA', 212, 'product', 'pudim-sabor-baunilha', '', 'cod-212.jpg', '12', '50G', '21069021', '36X50G', '26,5 X 17,5 X 18,5', '0,0086', '2,32', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(119, 'PUDIM SABOR CHOCOLATE', 211, 'product', 'pudim-sabor-chocolate', '', 'cod-211.jpg', '12', '50G', '21069021', '36X50G', '26,5 X 17,5 X 18,5', '0,0086', '2,32', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(120, 'PUDIM SABOR MORANGO', 213, 'product', 'pudim-sabor-morango', '', 'cod-213.jpg', '12', '50G', '21069021', '36X50G', '26,5 X 17,5 X 18,5', '0,0086', '2,32', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(121, 'AMENDOIM CRU', 19, 'product', 'amendoim-cru', '', 'cod-19.jpg', '12', '500G', '12024200', '24X500G', '30,5 X 40,0 X 11,0 ', '0,0134', '12,35', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(122, 'AMENOIM BRANCO RUNNER', 131, 'product', 'amenoim-branco-runner', '', 'cod-131.jpg', '12', '500G', '12024200', '24X500G', '30,0 X 47,0 X 13,0', '0,0183', '12,22', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(123, 'ERVILHA PARTIDA', 13, 'product', 'ervilha-partida', '', 'cod-13.jpg', '12', '500G', '7131090', '24X500G', '33,0 X 34,0 X 13,0', '0,0146', '12,3', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(124, 'FEIJÃO BRANCO', 17, 'product', 'feijao-branco', '', 'cod-17.jpg', '12', '500G', '7133329', '24X500G', '39,0 X 39,0 X 11,0', '0,0167', '12,42', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(125, 'GRÃO DE BICO', 20, 'product', 'grao-de-bico', '', 'cod-20.jpg', '18', '500G', '7089000', '24X500G', '30,0 X 44,0 X 11,0', '0,0145', '12,38', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(126, 'LENTILHA', 21, 'product', 'lentilha', '', 'cod-21.jpg', '18', '500G', '7134090', '24X500G', '36,0 X 40,0 X 12,0', '0,0173', '12,36', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(127, 'TRIGO EM GRÃOS', 401, 'product', 'trigo-em-graos', '', 'cod-401.jpg', '12', '500G', '11011900', '24X500G', '31,5 X 29,5 X 15,0', '0,0139', '6,42', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(128, 'AVEIA EM FLOCOS', 157, 'product', 'aveia-em-flocos', '', 'cod-157.jpg', '6', '200G', '11041200', '12X200G', '26,0 X 23,5 X 18,0 ', '0,011', '3,02', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(129, 'AVEIA EM FLOCOS FINOS', 158, 'product', 'aveia-em-flocos-finos', '', 'cod-158.jpg', '6', '200G', '11041200', '12X200G', '26,0 X 23,5 X 18,0 ', '0,011', '3,02', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(130, 'FARINHA DE AVEIA', 189, 'product', 'farinha-de-aveia', '', 'cod-189.jpg', '8', '200G', '11029000', '12X200G', '26,0 X 23,5 X 18,0 ', '0,011', '3,01', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(131, 'QUINOA REAL EM FLOCOS', 284, 'product', 'quinoa-real-em-flocos', '', 'cod-284.jpg', '12', '100G', '11041900', '12X100G', '26,5 X 23,5 X 18,0', '0,0112', '1,5', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(132, 'SEMENTES DE CHIA', 266, 'product', 'sementes-de-chia', '', 'cod-266.jpg', '12', '100G', '12079990', '12X100G', '26,0 X 23,5 X 18,0', '0,011', '1,72', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(133, 'AÇUCAR MASCAVO', 123, 'product', 'acucar-mascavo', '', 'cod-123.jpg', '12', '500G', '17019900', '12X500G', '31,5 X 29,5 X 15,0', '0,0139', '6,42', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(134, 'EXTRATO DE SOJA', 127, 'product', 'extrato-de-soja', '', 'cod-127.jpg', '12', '300G', '12081000', '12X300G', '29,5 X 15,0 X 31,5', '0,0139', '4,1', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(135, 'FARINHA DE SOJA TORRADA (KINAKO)', 178, 'product', 'farinha-de-soja-torrada-(kinako)', '', 'cod-178.jpg', '12', '300G', '12081000', '12X300G', '29,5 X 15,0 X 31,5', '0,0139', '6,59', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(136, 'FIBRA DE TRIGO', 174, 'product', 'fibra-de-trigo', '', 'cod-174.jpg', '8', '250G', '23023090', '12X250G', '29,5 X 15,0 X 31,5', '0,0139', '3,48', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(137, 'GERGILIM', 220, 'product', 'gergilim', '', 'cod-220.jpg', '12', '200G', '12074090', '12X200G', '32,0 X 21,0 X 16,0', '0,0108', '2,92', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(138, 'LINHAÇA DOURADA', 219, 'product', 'linhaca-dourada', '', 'cod-219.jpg', '12', '200G', '12040090', '12X200G', '32,0 X 21,0 X 16,0', '0,0108', '2,79', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(139, 'GRANOLA TRADICIONAL', 221, 'product', 'granola-tradicional', '', 'cod-221.jpg', '12', '50G', '19041000', '12X250G', '28,5 X 20,0 X 17,0', '0,0097', '3,1', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(140, 'GRANOLA BANANA COM CANELA', 224, 'product', 'granola-banana-com-canela', '', 'cod-224.jpg', '12', '50G', '19041000', '12X250G', '20,0 X 28,5 X 17,0', '0,0097', '3,1', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(141, 'GRANOLA COM CASTANHA', 311, 'product', 'granola-com-castanha', '', 'cod-311.jpg', '12', '50G', '19041000', '12X250G', '28,5 X 20,0 X 17,0', '0,0097', '3,1', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(142, 'GRANOLA CHOCOLATE', 312, 'product', 'granola-chocolate', '', 'cod-312.jpg', '12', '50G', '19041000', '12X250G', '28,5 X 20,0 X 17,0', '0,0097', '3,1', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(143, 'GRANOLA COM FRUTAS', 309, 'product', 'granola-com-frutas', '', 'cod-309.jpg', '12', '50G', '19041000', '12X250G', '28,5 X 20,0 X 17,0', '0,0097', '3,1', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(144, 'GRANOLA SEM AÇUCAR', 310, 'product', 'granola-sem-acucar', '', 'cod-310.jpg', '12', '50G', '19041000', '12X250G', '28,5 X 20,0 X 17,0', '0,0097', '3,1', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(145, 'SEMENTE DE LINHAÇA', 179, 'product', 'semente-de-linhaca', '', 'cod-179.jpg', '12', '200G', '12040090', '12X200G', '32,0 X 21,0 X 16,0', '0,0108', '2,79', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(146, 'PROTEÍNA DE SOJA TEXTURIZADA', 225, 'product', 'proteina-de-soja-texturizada', '', 'cod-225.jpg', '12', '250G', '21061000', '12X250G', '29,5 X 15,0 X 31,5', '0,0139', '3,18', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(147, 'SOJA EM GRÃO', 139, 'product', 'soja-em-grao', '', 'cod-139.jpg', '12', '500G', '12019000', '12X500G', '29,5 X 15,0 X 31,5', '0,0139', '6,47', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(148, 'AVEIA EM FLOCOS', 176, 'product', 'aveia-em-flocos-1', '', 'cod-176.jpg', '6', '400G', '11041200', '12X400G', '31,5 X 29,5 X 15,0', '0,0139', '5,32', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(149, 'SAL MOÍDO', 379, 'product', 'sal-moido', '', 'cod-379.jpg', '24', '1KG', '25010020', '30X1KG', '31,0 X 47,0 X 14,5', '0,0211', '31,64', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(150, 'ÓLEO DE COCO EXTRA VIRGEM', 396, 'product', 'oleo-de-coco-extra-virgem', '', 'cod-396.jpg', '24', '450G', '15131100', '12X250ML', '20,5 X 26,0 X 16,5', '0,0088', '5,67', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(151, 'ÓLEO DE COCO REFINADO SEM SABOR', 397, 'product', 'oleo-de-coco-refinado-sem-sabor', '', 'cod-397.jpg', '24', '450G', '15131900', '12X250ML', '20,5 X 26,0 X 16,5', '0,0088', '5,67', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(152, 'FARINHA DE ARROZ', 393, 'product', 'farinha-de-arroz', '', 'cod-393.jpg', '12', '250G', '11029000', '12X250G', '26,0 X 18,0 X 23,5', '0,011', '3,6', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(153, 'FARINHA DE BANANA VERDE', 394, 'product', 'farinha-de-banana-verde', '', 'cod-394.jpg', '12', '200G', '11063000', '12X200G', '26,0 X 18,0 X 23,5', '0,011', '3,01', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(154, 'FARINHA DE BERINJELA', 388, 'product', 'farinha-de-berinjela', '', 'cod-388.jpg', '12', '200G', '21069030', '12X200G', '26,0 X 18,0 X 23,5', '0,011', '3,01', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(155, 'FARINHA DE CHIA', 395, 'product', 'farinha-de-chia', '', 'cod-395.jpg', '12', '200G', '12089090', '12X200G', '26,0 X 18,0 X 23,5', '0,011', '3,01', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(156, 'FARINHA DE COCO', 389, 'product', 'farinha-de-coco', '', 'cod-389.jpg', '12', '200G', '11063000', '12X200G', '26,0 X 18,0 X 23,5', '0,011', '3,01', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(157, 'FARINHA DE CASCA DE MARACUJÁ', 391, 'product', 'farinha-de-casca-de-maracuja', '', 'cod-391.jpg', '12', '200G', '11063000', '12X200G', '26,0 X 18,0 X 23,5', '0,011', '3,01', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(158, 'FARINHA DE UVA', 392, 'product', 'farinha-de-uva', '', 'cod-392.jpg', '12', '200G', '11063000', '12X200G', '26,0 X 18,0 X 23,5', '0,011', '3,01', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(159, 'KETCHUP TRADICIONAL ANIÁ', 383, 'product', 'ketchup-tradicional-ania', '', 'cod-383.jpg', '15', '190G', '21032010', '24X190G', '29,0 X 19,5 X 17,0', '0,0096', '5,4', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(160, 'KETCHUP TRADICIONAL ANIÁ', 382, 'product', 'ketchup-tradicional-ania-1', '', 'cod-382.jpg', '15', '370G', '21032010', '24X370G', '35,0 X 23,5 X 21,5', '0,0177', '9,77', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(161, 'MOSTARDA TRADICIONAL ANIÁ', 384, 'product', 'mostarda-tradicional-ania', '', 'cod-384.jpg', '15', '190G', '21033021', '24X190G', '29,0 X 19,5 X 17,0', '0,0096', '5,4', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(162, 'MAIONESE TRADICIONAL', 385, 'product', 'maionese-tradicional', '', 'cod-385.jpg', '10', '200G', '21039011', '24X200G', '24,0 X 16,3 X 14,5', '0,0057', '16,3', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(163, 'CREME DE PIMENTA ABSOLUTA', 380, 'product', 'creme-de-pimenta-absoluta', '', 'cod-380.jpg', '24', '240G', '21039091', '12X170ML', '24,0 X 18,5 X 14,5', '0,0064', '2,78', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(164, 'MOLHO DE PIMENTA CALABRESA', 184, 'product', 'molho-de-pimenta-calabresa', '', 'cod-184.jpg', '24', '183G', '21039091', '12X150ML', '18,0 X 13,5 X 18,0', '0,0044', '2,3', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(165, 'MOLHO DE PIMENTA PARA CHURRASCO', 103, 'product', 'molho-de-pimenta-para-churrasco', '', 'cod-103.jpg', '24', '189G', '21039091', '12X150ML', '18,0 X 13,5 X 18,0', '0,0044', '13,5', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(166, 'MOLHO DE PIMENTA TRADICIONAL', 102, 'product', 'molho-de-pimenta-tradicional', '', 'cod-102.jpg', '24', '189G', '21039091', '12X150ML', '18,0 X 13,5 X 18,0', '0,0044', '2,31', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(167, 'MOLHO INGLÊS', 105, 'product', 'molho-ingles', '', 'cod-105.jpg', '24', '194G', '21039021', '12X150ML', '18,0 X 13,5 X 18,0', '0,0044', '2,44', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(168, 'MOLHO DE ALHO', 109, 'product', 'molho-de-alho', '', 'cod-109.jpg', '24', '190G', '21039091', '12X150ML', '18,0 X 13,5 X 18,0', '0,0044', '2,31', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(169, 'MOLHO DE SHOYU', 104, 'product', 'molho-de-shoyu', '', 'cod-104.jpg', '24', '195G', '21031010', '12X150ML', '18,0 X 13,5 X 18,0', '0,0044', '2,49', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(170, 'PATÊ DE ALHO', 353, 'product', 'pate-de-alho', '', 'cod-353.jpg', '12', '287G', '21039091', '12X234ML', '25,0 X 18,0 X 20,0', '0,009', '4,58', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(171, 'MOLHO DE BARBECUE', 299, 'product', 'molho-de-barbecue', '', 'cod-299.jpg', '12', '310G', '21039091', '12X234ML', '25,30 X 18,0 X 20,0', '0,0091', '3,78', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(172, 'MOLHO CHIMICHURRI CLÁSSICO', 298, 'product', 'molho-chimichurri-classico', '', 'cod-298.jpg', '12', '298G', '21039021', '12X234ML', '25,0 X 18,0 X 20,0', '0,009', '3,43', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(173, 'MOLHO PARA CHURRASCO TRADICIONAL', 345, 'product', 'molho-para-churrasco-tradicional', '', 'cod-345.jpg', '12', '300G', '21039091', '12X234ML', '25,0 X 18,0 X 20,0', '0,009', '3,46', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(174, 'MOLHO PARA CHURRASCO PICANTE', 352, 'product', 'molho-para-churrasco-picante', '', 'cod-352.jpg', '12', '287G', '21039091', '12X234ML', '25,0 X 18,0 X 20,0', '0,009', '3,55', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(175, 'MOLHO PARA SALADA CASEIRO', 294, 'product', 'molho-para-salada-caseiro', '', 'cod-294.jpg', '12', '279G', '21039021', '12X234ML', '25,0 X 18,0 X 20,0', '0,009', '3,41', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(176, 'MOLHO PARA SALADA ITALIANO', 295, 'product', 'molho-para-salada-italiano', '', 'cod-295.jpg', '12', '280G', '21039091', '12X234ML', '25,0 X 18,0 X 20,0', '0,009', '3,47', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(177, 'MOLHO PARA SALADA LIMÃO', 296, 'product', 'molho-para-salada-limao', '', 'cod-296.jpg', '12', '271G', '21039091', '12X234ML', '25,0 X 18,0 X 20,0', '0,009', '3,48', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(178, 'MOLHO PARA SALADA ROSÉ', 297, 'product', 'molho-para-salada-rose', '', 'cod-297.jpg', '12', '280G', '21039091', '12X234ML', '25,0 X 18,0 X 20,0', '0,009', '3,48', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(179, 'MOLHO DE PIMENTA CAIPIRA', 403, 'product', 'molho-de-pimenta-caipira', '', 'cod-403.jpg', '24', '181G', '21039091', '12X150ML', '19,5 X 14,5 X 13,5', '0,0038', '2,24', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(180, 'MOLHO DE PIMENTA MINEIRO', 404, 'product', 'molho-de-pimenta-mineiro', '', 'cod-404.jpg', '24', '180G', '21039091', '12X150ML', '19,5 X 14,5 X 13,5', '0,0038', '2,24', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(181, 'MOLHO DE PIMENTA MEXICANO', 407, 'product', 'molho-de-pimenta-mexicano', '', 'cod-407.jpg', '24', '183G', '21039021', '12X150ML', '19,5 X 14,5 X 13,5', '0,0038', '2,24', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(182, 'MOLHO DE PIMENTA VERDE CREMOSA', 405, 'product', 'molho-de-pimenta-verde-cremosa', '', 'cod-405.jpg', '18', '182G', '21039091', '12X150ML', '19,5 X 14,5 X 13,5', '0,0038', '2,24', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(183, 'MOLHO DE PIMENTA TRADICIONAL', 160, 'product', 'molho-de-pimenta-tradicional-1', '', 'cod-160.jpg', '24', '554G', '21039091', '12X500ML', '19,0 X 14,0 X 19,0', '0,0051', '6,92', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(184, 'MOLHO DE PIMENTA CASEIRO', 406, 'product', 'molho-de-pimenta-caseiro', '', 'cod-406.jpg', '24', '560G', '21039091', '12X500ML', '25,0 X 20,0 X 24,0', '0,012', '6,92', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(185, 'AZEITE DE DENDÊ', 331, 'product', 'azeite-de-dende', '', 'cod-331.jpg', '24', '230G', '15111000', '12X200ML', '23,0 X 18,0 X 15,0', '0,0062', '2,66', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(186, 'MOLHO SHOYU', 161, 'product', 'molho-shoyu', '', 'cod-161.jpg', '24', '611G', '21031010', '12X500ML', '19,0 X 14,0 X 19,0', '0,0051', '7,38', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(187, 'ALPISTE', 33, 'product', 'alpiste', '', 'cod-33.jpg', '24', '500G', '10083090', '24X500G', '30,0 x 48,0 x 16,5', '0,0238', '12,45', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(188, 'GIRASSOL', 34, 'product', 'girassol', '', 'cod-34.jpg', '24', '250G', '12060090', '24X250G', '30,0 X 40,0 X 12,0', '0,0144', '6,31', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(189, 'GIRASSOL', 159, 'product', 'girassol-1', '', 'cod-159.jpg', '24', '500G', '12060090', '24X500G', '40,0 X 56,0 X 14,0', '0,0314', '12,39', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(190, 'PAINÇO', 35, 'product', 'painco', '', 'cod-35.jpg', '24', '500g', '10082190', '24X500G', '30,0 x 39,0 x 14,0', '0,0164', '12,36', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(191, 'PEQUI FATIADO', 354, 'product', 'pequi-fatiado', '', 'cod-354.jpg', '12', '110G', '20019000', '12X325G', '25,0 X 19,0 X 16,0', '0,0076', '4,613', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(192, 'PIMENTA BIQUINHO', 337, 'product', 'pimenta-biquinho', '', 'cod-337.jpg', '12', '160G', '20019000', '12X160G', '25,0 X 19,0 X 16,0', '0,0076', '6,78', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(193, 'PIMENTA BODE', 340, 'product', 'pimenta-bode', '', 'cod-340.jpg', '12', '100G', '20019000', '12X100G', '13,0 X 18,5 X 20,0', '0,0048', '4,23', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(194, 'PIMENTA VERMELHA EM PASTA', 82, 'product', 'pimenta-vermelha-em-pasta', '', 'cod-82.jpg', '12', '300G', '20059900', '12X300G', '29,5 X 22,5 X 11,5', '0,0076', '4,06', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(195, 'PIMENTA HABANERO', 339, 'product', 'pimenta-habanero', '', 'cod-339.jpg', '12', '120G', '20019000', '12X120G', '25,0 X 19,0 X 16,0', '0,0076', '6,92', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(196, 'PIMENTA DEDO DE MOÇA', 344, 'product', 'pimenta-dedo-de-moca', '', 'cod-344.jpg', '12', '110G', '20019000', '12X110G', '25,0 X 19,0 X 16,0', '0,0076', '6,96', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(197, 'PIMENTA MALAGUETA', 342, 'product', 'pimenta-malagueta', '', 'cod-342.jpg', '12', '100G', '20019000', '12X100G', '13,0 X 18,5 X 20,0', '0,0048', '4,,19', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(198, 'PIMENTA CUMARI VERDE', 338, 'product', 'pimenta-cumari-verde', '', 'cod-338.jpg', '12', '35G', '20019000', '12X35G', '15,0 x 12,5 x 16,5', '0,0031', '2,47', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(199, 'PIMENTA CUMARI VERMELHA', 341, 'product', 'pimenta-cumari-vermelha', '', 'cod-341.jpg', '12', '35G', '20019000', '12X35G', '15,0 X 12,5 X 16,5', '0,0031', '2,47', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(200, 'POP CORN BACON', 150, 'product', 'pop-corn-bacon', '', 'cod-150.jpg', '12', '100G', '20081900', '24X100G', '37,0 X 25,0 X 12,0', '0,0111', '3,11', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(201, 'POP CORN CHOCOLATE', 304, 'product', 'pop-corn-chocolate', '', 'cod-304.jpg', '12', '100G', '20081900', '24X100G', '37,0 X 25,0 X 12,0', '0,0111', '3,11', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(202, 'POP CORN MANTEIGA DE CINEMA', 305, 'product', 'pop-corn-manteiga-de-cinema', '', 'cod-305.jpg', '12', '100G', '20081900', '24X100G', '37,0 X 25,0 X 12,0', '0,0111', '3,11', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(203, 'POP CORN MANTEIGA', 61, 'product', 'pop-corn-manteiga', '', 'cod-61.jpg', '12', '100G', '20081900', '24X100G', '37,0 X 25,0 X 12,0', '0,0111', '3,11', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(204, 'POP CORN NATURAL', 60, 'product', 'pop-corn-natural', '', 'cod-60.jpg', '12', '100G', '20081900', '24X100G', '37,0 X 25,0 X 12,0', '0,0111', '3,11', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(205, 'POP CORN QUEIJO', 151, 'product', 'pop-corn-queijo', '', 'cod-151.jpg', '12', '100G', '20081900', '24X100G', '37,0 X 25,0 X 12,0', '0,0111', '3,11', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(206, 'CALDO DE CARNES EM CUBOS', 348, 'product', 'caldo-de-carnes-em-cubos', '', 'cod-348.jpg', '12', '57G', '21041011', '16X10X57G', '28,0 X 19,5 X 22,0', '0,012', '10,24', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(207, 'CALDO DE BACON EM CUBOS', 346, 'product', 'caldo-de-bacon-em-cubos', '', 'cod-346.jpg', '12', '57G', '21041011', '16X10X57G', '28,0 X 19,5 X 22,0', '0,012', '10,24', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(208, 'CALDO DE GALINHA EM CUBOS', 349, 'product', 'caldo-de-galinha-em-cubos', '', 'cod-349.jpg', '12', '57G', '21041011', '16X10X57G', '28,0 X 19,5 X 22,0', '0,012', '10,24', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(209, 'CALDO DE LEGUMES EM CUBOS', 347, 'product', 'caldo-de-legumes-em-cubos', '', 'cod-347.jpg', '12', '57G', '21041011', '16X10X57G', '28,0 X 19,5 X 22,0', '0,012', '10,24', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(210, 'CALDO DE CARNE EM PÓ', 289, 'product', 'caldo-de-carne-em-po', '', 'cod-289.jpg', '12', '1,01KG', '21041019', '12X1,01KG', '30,0 X 36,0 X 15,0', '0,0162', '12,18', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(211, 'CALDO DE GALINHA EM PÓ', 288, 'product', 'caldo-de-galinha-em-po', '', 'cod-288.jpg', '12', '1,01KG', '21041019', '12X1,01KG', '30,0 X 36,0 X 15,0', '0,0162', '12,18', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(212, 'CALDO DE LEGUMES EM PÓ', 287, 'product', 'caldo-de-legumes-em-po', '', 'cod-287.jpg', '12', '1,01KG', '21041019', '12X1,01KG', '30,0 X 36,0 X 15,0', '0,0162', '12,18', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(213, 'CREME DE CEBOLA', 375, 'product', 'creme-de-cebola', '', 'cod-375.jpg', '12', '1,01KG', '21041019', '12X1,01KG', '39,0 X 41,0 X 14,0', '0,0224', '12,18', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(214, 'CREME DE CEBOLA', 292, 'product', 'creme-de-cebola-1', '', 'cod-292.jpg', '12', '65G', '21041011', '4X12X65G', '12,0 X 18,0 X 15,5', '0,0033', '0,79', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(215, 'CREME DE GALINHA', 293, 'product', 'creme-de-galinha', '', 'cod-293.jpg', '12', '65G', '21041011', '4X12X65G', '51,0 X 19,0 X 16,5', '0,016', '3,22', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(216, 'MOEDOR MIX DE PIMENTAS', 357, 'product', 'moedor-mix-de-pimentas', '', 'cod-357.jpg', '24', '50G', '9041100', '6X50G', '17,0 X 12,0 X 14,0', '0,0029', '1,23', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(217, 'MOEDOR PIMENTA-DO-REINO EM GRÃOS', 359, 'product', 'moedor-pimenta-do-reino-em-graos', '', 'cod-359.jpg', '24', '50G', '9041100', '6X50G', '17,0 X 12,0 X 14,0', '0,0029', '1,23', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(218, 'MOEDOR SAL DO HIMALAYA', 360, 'product', 'moedor-sal-do-himalaya', '', 'cod-360.jpg', '24', '100G', '25010090', '6X100G', '17,0 X 12,0 X 14,0', '0,0029', '1,31', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(219, 'MOEDOR SAL DO HIMALAYA C/ ALHO, CEBOLA E SALSA', 355, 'product', 'moedor-sal-do-himalaya-c-alho-cebola-e-salsa', '', 'cod-355.jpg', '24', '95G', '25010090', '6X95G', '17,0 x 12,0 x 14,0', '0,0029', '1,6', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(220, 'MOEDOR SAL DO HIMALAYA C/ ERVAS FINAS', 356, 'product', 'moedor-sal-do-himalaya-c-ervas-finas', '', 'cod-356.jpg', '24', '100G', '25010090', '6X100G', '17,0 x 12,0 x 14,0', '0,0029', '1,6', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(221, 'MOEDOR SAL DO HIMALAYA C/ PIMENTA CALABRESA', 358, 'product', 'moedor-sal-do-himalaya-c-pimenta-calabresa', '', 'cod-358.jpg', '24', '100G', '25010090', '6X100G', '17,0 x 12,0 x 14,0', '0,0029', '1,6', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(222, 'REFIL P/ MOEDOR MIX DE PIMENTA', 364, 'product', 'refil-p-moedor-mix-de-pimenta', '', 'cod-364.jpg', '24', '50G', '9041100', '6X50G', '17,0 X 11,5 X 14,0', '0,0027', '0,36', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08');
INSERT INTO `contents` (`id`, `title`, `sku`, `type`, `url`, `conjunto`, `image`, `validade`, `peso_unitario`, `ncm`, `qtd_caixa`, `cla`, `m3_caixa`, `peso_caixa`, `container`, `lang`, `created_at`, `updated_at`) VALUES
(223, 'REFIL P/ MOEDOR PIMENTA-DO-REINO EM GRÃO', 366, 'product', 'refil-p-moedor-pimenta-do-reino-em-grao', '', 'cod-366.jpg', '24', '50G', '9041100', '6X50G', '17,0 X 11,5 X 14,0', '0,0027', '0,36', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(224, 'REFIL P/ MOEDOR SAL DO HIMALAYA', 367, 'product', 'refil-p-moedor-sal-do-himalaya', '', 'cod-367.jpg', '24', '100G', '25010090', '6X100G', '17,0 X 11,5 X 14,0', '0,0027', '0,77', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(225, 'SAL ROSA DO HIMALAYA MOIDO', 387, 'product', 'sal-rosa-do-himalaya-moido', '', 'cod-387.jpg', '24', '400G', '25010090', '12X500G', '17,5 X 21,5 X 16,0', '0,006', '4,98', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(226, 'REFIL PARA MOEDOR SAL DO HIMALAYA COM ALHO CEBOLA E SALSA', 362, 'product', 'refil-para-moedor-sal-do-himalaya-com-alho-cebola-e-salsa', '', 'cod-362.jpg', '24', '95G', '25010090', '6X95G', '17,0 X 11,5 X 14,0', '0,0027', '0,68', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(227, 'REFIL PARA MOEDOR SAL DO HIMALAYA COM ERVAS FINAS', 363, 'product', 'refil-para-moedor-sal-do-himalaya-com-ervas-finas', '', 'cod-363.jpg', '24', '100G', '25010090', '6X100G', '17,0 X 11,5 X 14,0', '0,0027', '0,77', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(228, 'REFIL PARA MOEDOR SAL DO HIMALAYA COM PIMENTA CALABRASA', 365, 'product', 'refil-para-moedor-sal-do-himalaya-com-pimenta-calabrasa', '', 'cod-365.jpg', '24', '100G', '25010090', '6X100G', '17,0 X 11,5 X 14,0', '0,0027', '0,71', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(229, 'TEMPERO ALHO E SAL', 78, 'product', 'tempero-alho-e-sal', '', 'cod-78.jpg', '24', '400G', '21039021', '12X400G', '21,5 X 17,5 X 16,0', '0,006', '5,11', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(230, 'TEMPERO COMPLETO COM AÇAFRÃO', 186, 'product', 'tempero-completo-com-acafrao', '', 'cod-186.jpg', '24', '400G', '21039021', '12X400G', '21,5 X 17,5 X 16,0', '0,006', '5,12', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(231, 'TEMPERO PARA CHURRASCO', 79, 'product', 'tempero-para-churrasco', '', 'cod-79.jpg', '24', '400G', '21039021', '12X400G', '21,5 X 17,5 X 16,0', '0,006', '400G', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(232, 'TEMPERO COMPLETO COM PIMENTA', 76, 'product', 'tempero-completo-com-pimenta', '', 'cod-76.jpg', '24', '400G', '21039021', '12X400G', '21,5 X 17,5 X 21,5', '0,0081', '5,11', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(233, 'TEMPERO COMPLETO SEM PIMENTA', 77, 'product', 'tempero-completo-sem-pimenta', '', 'cod-77.jpg', '24', '400G', '21039021', '12X400G', '21,5 X 17,5 X 16,0', '0,006', '5,11', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(234, 'TEMPERO COMPLETO C/ ERVAS FINAS', 187, 'product', 'tempero-completo-c-ervas-finas', '', 'cod-187.jpg', '24', '400G', '21039021', '12X400G', '21,5 X 17,5 X 16,0', '0,006', '5,12', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(235, 'TEMPERO ALHO E SAL', 243, 'product', 'tempero-alho-e-sal-1', '', 'cod-243.jpg', '24', '1,01KG', '21039029', '12X1,01KG', '35,0 X 24,0 X 29,0', '0,0244', '13,04', 'CAIXA', 'br', '2019-12-04 19:57:08', '2019-12-04 19:57:08'),
(236, 'TEMPERO ALHO E SAL', 140, 'product', 'tempero-alho-e-sal-2', '', 'cod-140.jpg', '24', '280G', '21039021', '24X280G', '28,0 X 22,0 X 20,0', '0,0123', '7,26', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(237, 'TEMPERO COMPLETO COM PIMENTA', 244, 'product', 'tempero-completo-com-pimenta-1', '', 'cod-244.jpg', '24', '1,01KG', '21039029', '12X1,01KG', '35,0 X 24,0 X 29,0', '0,0244', '13,04', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(238, 'TEMPERO COMPLETO COM PIMENTA', 141, 'product', 'tempero-completo-com-pimenta-2', '', 'cod-141.jpg', '24', '280G', '21039021', '24X280G', '28,0 X 22,0 X 20,0', '0,0123', '7,26', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(239, 'TEMPERO COMPLETO SEM PIMENTA', 255, 'product', 'tempero-completo-sem-pimenta-1', '', 'cod-255.jpg', '24', '1,01KG', '21039029', '12X1,01KG', '35,0 X 24,0 X 29,0', '0,0244', '13,04', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(240, 'TEMPERO COMPLETO SEM PIMENTA', 142, 'product', 'tempero-completo-sem-pimenta-2', '', 'cod-142.jpg', '24', '280G', '21039021', '24X280G', '28,0 X 22,0 X 20,0', '0,0123', '7,26', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(241, 'ALHO E CEBOLA TRITURADO S/SAL', 97, 'product', 'alho-e-cebola-triturado-s-sal', '', 'cod-97.jpg', '8', '300G', '20059900', '12X300G', '29,5 X 22,5 X 11,5', '0,0076', '5,15', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(242, 'PURO ALHO TRITURADO S/SAL', 80, 'product', 'puro-alho-triturado-s-sal', '', 'cod-80.jpg', '8', '300G', '20059900', '12X300G', '29,5 X 22,5 X 11,5', '0,0076', '5,15', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(243, 'PURO ALHO TRITURADO S/SAL', 209, 'product', 'puro-alho-triturado-s-sal-1', '', 'cod-209.jpg', '8', '1,01KG', '7129090', '6X1,01KG', '39,0 X 13,5 X 25,5', '0,0134', '6,58', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(244, 'PURO ALHO TRITURADO S/SAL', 233, 'product', 'puro-alho-triturado-s-sal-2', '', 'cod-233.jpg', '8', '3,5KG', '7129090', '1X3,5KG', '-', '', '3,5', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(245, 'TEMPERO ESPECIAL ALHO, CEBOLA E SALSA', 112, 'product', 'tempero-especial-alho-cebola-e-salsa', '', 'cod-112.jpg', '12', '20G', '21039021', '24X20G', '20,0 X 16,0 X 9,0', '0,0029', '0,7', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(246, 'TEMPERO ESPECIAL BAIANO', 73, 'product', 'tempero-especial-baiano', '', 'cod-73.jpg', '12', '30G', '21039021', '24X30G', '22,0 X 17,5 X 13,0', '0,005', '0,91', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(247, 'TEMPERO ESPECIAL CHIMICHURRI', 90, 'product', 'tempero-especial-chimichurri', '', 'cod-90.jpg', '12', '15G', '21039021', '24X15G', '16,0 X 8,0 X 16,0', '0,002', '0,54', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(248, 'TEMPERO ESPECIAL CURRY', 371, 'product', 'tempero-especial-curry', '', 'cod-371.jpg', '12', '20G', '21039021', '24X20G', '20,0 X 8,0 X 9,0', '0,0014', '0,55', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(249, 'TEMPERO ESPECIAL ERVAS FINAS', 89, 'product', 'tempero-especial-ervas-finas', '', 'cod-89.jpg', '12', '10G', '21039021', '24X10G', '21,0 X 16,0 X 10,0', '0,0034', '0,42', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(250, 'TEMPERO ESPECIAL NORDESTE', 370, 'product', 'tempero-especial-nordeste', '', 'cod-370.jpg', '12', '20G', '21039021', '24X20G', '20,0 X 8,0 X 9,0', '0,0014', '0,55', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(251, 'TEMPERO ESPECIAL AVES', 115, 'product', 'tempero-especial-aves', '', 'cod-115.jpg', '12', '30G', '21039021', '24X30G', '22,0 X 17,5 X 13,0', '0,005', '0,93', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(252, 'TEMPERO ESPECIAL CARNEIRO', 369, 'product', 'tempero-especial-carneiro', '', 'cod-369.jpg', '12', '20G', '21039021', '24X20G', '18,0 X 10,0 X 9,0', '0,0016', '0,55', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(253, 'TEMPERO ESPECIAL CARNES', 114, 'product', 'tempero-especial-carnes', '', 'cod-114.jpg', '12', '30G', '21039021', '24X30G', '22,0 X 17,5 X 13,0', '0,005', '0,92', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(254, 'TEMPERO ESPECIAL PEIXES', 113, 'product', 'tempero-especial-peixes', '', 'cod-113.jpg', '12', '30G', '21039021', '24X30G', '22,0 X 17,5 X 13,0', '0,005', '0,91', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(255, 'TEMPERO ESPECIAL PIZZA', 74, 'product', 'tempero-especial-pizza', '', 'cod-74.jpg', '12', '10G', '21039021', '24X10G', '22,0 X 13,0 X 9,0', '0,0026', '4,03', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(256, 'TEMPERO ESPECIAL VINAGRETE', 185, 'product', 'tempero-especial-vinagrete', '', 'cod-185.jpg', '12', '20G', '21039021', '24X20G', '20,0 X 19,0 X 9,0', '0,0034', '0,68', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(257, 'TEMPERO ESPECIAL TOMATE, CEBOLA E SALSA', 144, 'product', 'tempero-especial-tomate-cebola-e-salsa', '', 'cod-144.jpg', '12', '15G', '21039021', '24X15G', '20,0 X 17,5 X 9,0', '0,0032', '0,55', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(258, 'TEMPERO PRÁTICO SABOR ALHO', 376, 'product', 'tempero-pratico-sabor-alho', '', 'cod-376.jpg', '12', '50G', '21039021', '72X50G', '18,0 X 38,0 X 19,5', '0,0133', '4,46', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(259, 'TEMPERO PRÁTICO SABOR P/ AVES, PEIXES E ARROZ', 231, 'product', 'tempero-pratico-sabor-p-aves-peixes-e-arroz', '', 'cod-231.jpg', '12', '50G', '21039021', '72X50G', '18,0 X 38,0 X 19,5', '0,0133', '4,46', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(260, 'TEMPERO PRÁTICO SABOR P/ CARNES, LEGUMES E ARROZ', 230, 'product', 'tempero-pratico-sabor-p-carnes-legumes-e-arroz', '', 'cod-230.jpg', '12', '50G', '21039021', '72X50G', '18,0 X 38,0 X 19,5', '0,0133', '4,46', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(261, 'TEMPERO PRÁTICO SABOR P/ FEIJÃO, OVOS E ARROZ', 227, 'product', 'tempero-pratico-sabor-p-feijao-ovos-e-arroz', '', 'cod-227.jpg', '12', '50G', '21039021', '72X50G', '18,0 X 38,0 X 19,5', '0,0133', '4,46', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(262, 'TEMPERO PRÁTICO SABOR P/ LEGUMES, VERDURAS E ARROZ', 226, 'product', 'tempero-pratico-sabor-p-legumes-verduras-e-arroz', '', 'cod-226.jpg', '12', '50G', '21039021', '72X50G', '18,0 X 38,0 X 19,5', '0,0133', '4,46', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(263, 'TEMPERO PRÁTICO SABOR P/ MASSA, BATATAS E ARROZ', 228, 'product', 'tempero-pratico-sabor-p-massa-batatas-e-arroz', '', 'cod-228.jpg', '12', '50G', '21039021', '72X50G', '18,0 X 38,0 X 19,5', '0,0133', '4,46', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(264, 'TEMPEROS PRÁTICO SABOR DO NORDESTE', 277, 'product', 'temperos-pratico-sabor-do-nordeste', '', 'cod-277.jpg', '12', '50G', '21039021', '72X50G', '18,0 X 38,0 X 19,5', '0,0133', '4,46', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(265, 'TEMPERO PRÁTICO SABOR P/ SALADAS', 229, 'product', 'tempero-pratico-sabor-p-saladas', '', 'cod-229.jpg', '12', '50G', '21039021', '72X50G', '18,0 X 38,0 X 19,5', '0,0133', '4,46', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(266, 'TEMPERO PRÁTICO SABOR P/ CARNES, LEGUMES E ARROZ', 290, 'product', 'tempero-pratico-sabor-p-carnes-legumes-e-arroz-1', '', 'cod-290.jpg', '12', '1,01KG', '21039029', '12X1,01KG', '37,0 X 30,0 X 15,0', '0,0167', '12,17', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(267, 'TAPIOCA HIDRATADA', 308, 'product', 'tapioca-hidratada-1', '', 'cod-308.jpg', '6', '800G', '19030000', '12X800G', '44,0 X 29,5 X 17,0', '0,0221', '10,15', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(268, 'LINHAÇA MOÍDA', 180, 'product', 'linhaca-moida', '', 'cod-180.jpg', '12', '200G', '12040090', '12X200G', '32,0 X 21,0 X 16,0', '0,0108', '2,79', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(269, 'TEMPERO ESPECIAL ALHO, CEBOLA E SALSA', 245, 'product', 'tempero-especial-alho-cebola-e-salsa-1', '', 'cod-245.jpg', '12', '1KG', '21039021', '1X1KG', '34,0 X 5,0 X 39,0', '0,0066', '1,04', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(270, 'TEMPERO ESPECIAL TOMATE, CEBOLA E SALSA', 253, 'product', 'tempero-especial-tomate-cebola-e-salsa-1', '', 'cod-253.jpg', '12', '1KG', '21039021', '1X1KG', '34,0 X 5,0 X 39,0', '0,0066', '1,04', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(271, 'TEMPERO ESPECIAL VINAGRETE', 254, 'product', 'tempero-especial-vinagrete-1', '', 'cod-254.jpg', '12', '1KG', '21039021', '1X1KG', '34,0 X 5,0 X 39,0', '0,0066', '1,04', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(272, 'SAL DE PARRILLA TRADICIONAL 6X650G', 411, 'product', 'sal-de-parrilla-tradicional-6x650g', '', 'cod-411.jpg', '24', '650G', '25010090', '6X650G', '15,0 X 22,5 X 15,0', '0,0051', '4,58', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(273, 'SAL DE PARRILLA SUINO 6X400G', 415, 'product', 'sal-de-parrilla-suino-6x400g', '', 'cod-415.jpg', '24', '400G', '21039021', '6X400G', '12,0 X 22,0 X 15,0', '0,0041', '2,96', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(274, 'SAL DE PARRILLA CORDEIRO 6X400G', 414, 'product', 'sal-de-parrilla-cordeiro-6x400g', '', 'cod-414.jpg', '24', '400G', '21039021', '6X400G', '12,0 X 22,0 X 15,0', '0,0041', '3,19', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(275, 'SAL DE PARRILLA LEMON PEPPER 6X400G', 413, 'product', 'sal-de-parrilla-lemon-pepper-6x400g', '', 'cod-413.jpg', '24', '400G', '21039021', '6X400G', '12,0 X 22,0 X 15,0', '0,0041', '2,96', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(276, 'TEMPERO ESPECIAL LEMON PEPPER', 409, 'product', 'tempero-especial-lemon-pepper', '', 'cod-409.jpg', '12', '20G', '21039021', '24X20G', '20,0 X 8,0 X 9,0', '0,0014', '0,55', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(277, 'TEMPERO ESPECIAL ZAATAR', 410, 'product', 'tempero-especial-zaatar', '', 'cod-410.jpg', '12', '20G', '21039021', '24X20G', '20,0 X 8,0 X 9,0', '0,0014', '0,55', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(278, 'CANELA EM PÓ', 23, 'product', 'canela-em-po', '', 'cod-23.jpg', '12', '30G', '9062000', '24X30G', '13,0 X 17,50 X 13,0', '0,003', '0,93', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(279, 'CANELA EM RAMA 24 X 20G', 42, 'product', 'canela-em-rama-24-x-20g', '', 'cod-42.jpg', '24', '20G', '9061100', '24X20G', '20,0 X 14,0 X 8,0', '0,0022', '6,86', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(280, 'CEBOLINHA DESIDRATADA', 246, 'product', 'cebolinha-desidratada-1', '', 'cod-246.jpg', '12', '1KG', '7129090', '1X1KG', '34,0 X 5,0 X 39,0', '0,0066', '1,04', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(281, 'CHEIRO VERDE', 247, 'product', 'cheiro-verde-1', '', 'cod-247.jpg', '12', '1KG', '7129090', '1X1KG', '34,0 X 5,0 X 39,0', '0,0066', '1,03', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(282, 'LOURO EM FOLHAS', 248, 'product', 'louro-em-folhas-1', '', 'cod-248.jpg', '24', '1KG', '9109900', '1X1KG', '28,0 X 7,0 X 36,0', '0,0071', '1,04', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(283, 'MANJERICÃO', 249, 'product', 'manjericao-1', '', 'cod-249.jpg', '12', '1KG', '12119090', '1X1KG', '34,0 X 5,0 X 39,0', '0,0066', '1,04', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(284, 'PIMENTA CALABRESA', 251, 'product', 'pimenta-calabresa-1', '', 'cod-251.jpg', '12', '1KG', '9042100', '1X1KG', '34,0 X 5,0 X 39,0', '0,0066', '1,04', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(285, 'SALSA DESIDRATADA', 252, 'product', 'salsa-desidratada-1', '', 'cod-252.jpg', '12', '1KG', '7129090', '1X1KG', '34,0 X 5,0 X 39,0', '0,0066', '1,04', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(286, 'GENGIBRE', 412, 'product', 'gengibre', '', 'cod-412.jpg', '12', '20G', '9101100', '24X20G', '19,5 X 5,0 X 9,0', '0,0009', '0,67', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(287, 'MANJERONA', 169, 'product', 'manjerona', '', 'cod-169.jpg', '12', '10G', '12119090', '6X10G', '12,5 X 8,0 X 12,0', '0,0012', '0,24', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(288, 'SALSA DESIDRATADA', 171, 'product', 'salsa-desidratada-2', '', 'cod-171.jpg', '12', '8G', '7129090', '6X8G', '12,5 X 8,0 X 12,0', '0,0012', '0,23', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09'),
(289, 'BATATA PALHA EXTRA FINA', 408, 'product', 'batata-palha-extra-fina', '', 'cod-273.jpg', '6', '120G', '20052000', '26X120G', '42,0 X 26,0 X 26,5', '0,0289', '3,48', 'CAIXA', 'br', '2019-12-04 19:57:09', '2019-12-04 19:57:09');

-- --------------------------------------------------------

--
-- Estrutura para tabela `contents_has_contents`
--

CREATE TABLE `contents_has_contents` (
  `contents_id` int(11) NOT NULL,
  `contents_child_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `contents_images`
--

CREATE TABLE `contents_images` (
  `id` int(11) NOT NULL,
  `description` text,
  `type` varchar(45) DEFAULT NULL,
  `order` varchar(45) DEFAULT NULL,
  `image` text,
  `contents_id` int(11) NOT NULL,
  `path` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `informations`
--

CREATE TABLE `informations` (
  `id` int(11) NOT NULL,
  `address` text,
  `number` varchar(45) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `zipcode` varchar(45) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `whatsapp` varchar(45) DEFAULT NULL,
  `instagram` text,
  `facebook` text,
  `linkedin` text,
  `twitter` text,
  `pinterest` text,
  `phone1` varchar(45) DEFAULT NULL,
  `email` text,
  `phone2` varchar(45) DEFAULT NULL,
  `meta_description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `informations`
--

INSERT INTO `informations` (`id`, `address`, `number`, `district`, `zipcode`, `city`, `state`, `whatsapp`, `instagram`, `facebook`, `linkedin`, `twitter`, `pinterest`, `phone1`, `email`, `phone2`, `meta_description`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A Kinino é uma empresa brasileira, fundada em 1995, em São José do Rio Preto, interior de São Paulo. Atua na fabricação de itens de condimentos, especiarias, temperos, molhos, e muito mais. Tem como principal compromisso a qualidade na fabricação dos produtos. É uma das empresas líderes do segmento no interior de São Paulo.');

-- --------------------------------------------------------

--
-- Estrutura para tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Agencia LED', 'digital@agencialed.com.br', NULL, '$2y$10$4zDQi7W02JBh7cyn130R9uFSoQrPO2gjDDMjQy.nkp4EUYgB2ZKVG', 'OEy7pYMXStAMUj8rT4pEYsdgYUMOTEsFiLGlegDPdh05e037whGgttevZSaq', '2019-12-02 15:12:30', '2019-12-02 15:12:30');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `categories_has_contents`
--
ALTER TABLE `categories_has_contents`
  ADD PRIMARY KEY (`categories_id`,`contents_id`),
  ADD KEY `fk_categories_has_contents_contents1_idx` (`contents_id`),
  ADD KEY `fk_categories_has_contents_categories1_idx` (`categories_id`);

--
-- Índices de tabela `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url_UNIQUE` (`url`);

--
-- Índices de tabela `contents_has_contents`
--
ALTER TABLE `contents_has_contents`
  ADD PRIMARY KEY (`contents_id`,`contents_child_id`),
  ADD KEY `fk_contents_has_contents_contents2_idx` (`contents_child_id`),
  ADD KEY `fk_contents_has_contents_contents1_idx` (`contents_id`);

--
-- Índices de tabela `contents_images`
--
ALTER TABLE `contents_images`
  ADD PRIMARY KEY (`id`,`contents_id`),
  ADD KEY `fk_contents_images_contents_idx` (`contents_id`);

--
-- Índices de tabela `informations`
--
ALTER TABLE `informations`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Índices de tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de tabela `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de tabela `contents`
--
ALTER TABLE `contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=290;

--
-- AUTO_INCREMENT de tabela `contents_images`
--
ALTER TABLE `contents_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `informations`
--
ALTER TABLE `informations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `categories_has_contents`
--
ALTER TABLE `categories_has_contents`
  ADD CONSTRAINT `fk_categories_has_contents_categories1` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_categories_has_contents_contents1` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `contents_has_contents`
--
ALTER TABLE `contents_has_contents`
  ADD CONSTRAINT `fk_contents_has_contents_contents1` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_contents_has_contents_contents2` FOREIGN KEY (`contents_child_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `contents_images`
--
ALTER TABLE `contents_images`
  ADD CONSTRAINT `fk_contents_images_contents` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
