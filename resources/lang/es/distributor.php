<?php

return [
    'raiz-title' => 'Departamento de Exportaciones',
    'title' => 'Sea un distribuidor',
    'text' => '
        <p>Kinino es una empresa brasileña, fundada en 1995, en la ciudad de São José do Rio Preto/SP. Inicialmente, la línea de productos contaba, principalmente, con farináceos.</p> 
        <p>Siguiendo el crecimiento del mercado consumidor y atendiendo a sus necesidades ,  Kinino continuo ampliando el portafolio de productos y que hoy cuenta con mas de 300 items. Entre ellos: condimentos y especias , sazonadores, salsas, ajíes, farináceos, palomitas de maíz , tés y mates, postres, dulces, granos, alimentos para aves y productos naturales. Y nuevos productos son constantemente lanzados, tras ser  desarrollados y probados por el equipo de ingenieros de alimentos. Esta ampliación elevó la producción de Kinino. Hoy la capacidad productiva llega a 1.500 toneladas mensuales.</p>
        <p>En una fábrica nueva y moderna, compuesta por un área de 15 mil metros cuadrados, la producción es hecha por equipos automatizados y por colaboradores frecuentemente entrenados para realizar las actividades.</p>
        <p>Lleve el sabor de buenos negocios para  su país. Hable con  Kinino.</p>
    ',
];


