<?php

return [
    'home'=>'Inicio',
    'search' => 'Buscar',
    'products' => 'Productos',
    'distribution' => 'Conviértete en un Distribuidor',
    'contact' => 'Contacto',

    'linha1' => 'Conoce nuestras',
    'linha2' => 'Líneas de Productos'
];
