<?php

return [
    'raiz-title' => 'Export Departament',
    'title' => 'Be our distributor',

    'text' => ' 
        <p>Kinino is a Brazilian company founded in 1995 in São José do Rio Preto, in the state of São Paulo. The product range was mostly composed of flours in the beginning.</p> 
        <p>Following the growth of the consumer market and meeting its needs, Kinino continued to expand its product portofolio, which has more than 300 items now. Among them: condiments and spices, seasoning mixes, sauces, peppers, flours, popcorn, teas, desserts, sweets, grains, bird food and natural products. New products are constantly being launched after our team of food engineers develops and tests them. This expansion increased Kinino’s production and now the production capacity reaches 1,500 tonnes per month.</p>
        <p>In a new and moder factory, comprising an area of 15 thousand square meters, production is carried out by automated equipment and by employees who are frequently trained to carry out the activities.</p>
        <p>Bring the taste of good business to your country. Talk to Kinino.</p>
    ',

    'company' => 'Company',
    'subject' => 'Subject',
    'message' => 'Message',
    'send' => 'Send'
];
