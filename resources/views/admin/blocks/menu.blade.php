<div class="sidebar-header">
    <div class="sidebar-title">
        Menu Principal
    </div>
    <div class="sidebar-toggle d-none d-md-block" data-toggle-class="sidebar-left-collapsed" data-target="html"
        data-fire-event="sidebar-left-toggle">
        <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
    </div>
</div>

<div class="nano">
    <div class="nano-content">
        <nav id="menu" class="nav-main" role="navigation">
            <ul class="nav nav-main">
                <li class="nav-active">
                    <a class="nav-link" href="{{ route('information.index') }}">
                        <i class="fas fa-info"></i>
                        <span>Informações Gerais</span>
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('banners.index') }}">
                        <i class="far fa-images"></i>
                        <span>Banner Slide</span>
                    </a>
                </li>
                <li class="nav-parent">
                    <a class="nav-link" href="javascript:;">
                        <i class="fa fa-box" aria-hidden="true"></i>
                        <span>Categorias</span>
                    </a>
                    <ul class="nav nav-children">
                        <li>
                            <a class="nav-link" href="{{ route('categories.index', ['_type' => 'en']) }}">
                                <span>Inglês</span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{ route('categories.index', ['_type' => 'es']) }}">
                                <span>Espanhol</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a class="nav-link" href="javascript:;">
                        <i class="fa fa-box" aria-hidden="true"></i>
                        <span>Produtos</span>
                    </a>
                    <ul class="nav nav-children">
                        <li>
                            <a class="nav-link" href="{{ route('content.en.index',['_type'=>'en']) }}">
                                <span>Inglês</span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{ route('content.es.index',['_type'=>'es']) }}">
                                <span>Espanhol</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <hr class="separator" />
    </div>
</div>
