<header>
    <div class="container-fluid topbar">
        <div class="row">
            <div class="col-md-11 col-sm-12">
                <a href="{{ route('nav.index', ['locale' => App::getLocale()]) }}">
                    <img class="logo" src="{{ asset('img/logo.png') }}" alt="">
                </a>
                <div class="language-select float-right">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href="{{ route(Route::currentRouteName(), 'es') }}">
                                <img src="{{ asset('img/es_ES.png') }}" alt="">
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="{{ route(Route::currentRouteName(), 'en') }}">
                                <img src="{{ asset('img/en_US.png') }}" alt="">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <nav class="navbar navbar-expand-lg">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars" style="color:#000; font-size:28px;"></i>
        </button>
        <div class="container">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link {{ Route::is('nav.index', ['locale' => App::getLocale()])?'active':'' }}" 
                            href="{{ route('nav.index', ['locale' => App::getLocale()]) }}">
                            @lang('site.home')
                        </a>
                    </li>
                    <li class="nav-item">
                        <a id="lista" class="nav-link" 
                            href="{{ route('nav.index', ['locale' => App::getLocale()]) }}#itens">
                            @lang('site.products')
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Route::is('nav.contato')?'active':'' }}" 
                            href="{{ route('nav.contato', ['locale' => App::getLocale()]) }}">
                                @lang('site.distribution')
                            </a>
                    </li>
                    <li class="nav-item">
                        <a id="contato" class="nav-link" 
                            href="{{ route('nav.contato', ['locale' => App::getLocale()]) }}#formulario">
                            @lang('site.contact')
                        </a>
                    </li>
                </ul>
                <form action="{{ route('nav.procurar', ['locale' => App::getLocale()]) }}" id="search-form" method="get" class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" name="s" type="search" placeholder="@lang('site.search')..." required
                        aria-label="Search">
                    <button class="btn btn-default my-2 my-sm-0" type="submit">@lang('site.search')</button>
                </form>
            </div>
        </div>
    </nav>
</header>
