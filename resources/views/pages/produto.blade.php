@extends('layouts.default')

@section('social-tags')

    <meta property="og:title" content="Kinino Export">
    <meta property="og:description" content="{{ $produto->title }} - Kinino Export">
    <meta property="og:image" content="{{ asset('img/images/prod/'.$produto->image) }}">
    <meta property="og:image:alt" content="{{ asset('img/logo.png') }}">

    <meta property="og:image:width" content="1000"/>
    <meta property="og:image:height" content="1000"/>
    <meta property="og:url" content="{{ route('nav.produto', ['locale' => App::getLocale(), 'url' => $produto->url]) }}">

    <meta name="twitter:title" content="Kinino Export">
    <meta name="twitter:description" content="{{ $produto->title }} - Kinino Export">
    <meta name="twitter:image"content="{{ asset('img/images/prod/'.$produto->image) }}">
    <meta name="twitter:card" content="summary_large_image">

@endsection

@section('content')

<section class="products">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ route('nav.index', ['locale' => App::getLocale()]) }}">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a
                                href="{{ route('nav.produtos', ['locale' => App::getLocale(), 'categoria' => $categoria->url]) }}">{{ $categoria->title }}</a>
                        <li class="breadcrumb-item active" aria-current="page">{{ $produto->title }}</li>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-6">
                <img id="product-image-change" src="{{ asset('img/produtos/'.$produto->image) }}" class="img-fluid">
            </div>
            <div class="col-sm-12 col-md-12 col-lg-6 desc-produto">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="product-title">{{ $produto->title }}</h4>
                    </div>
                </div>

                <div><strong>Cod.: </strong>{{ $produto->sku }}</div>
                <div><strong>Validade Mês: </strong>{{ $produto->validade }}</div>
                <div><strong>Peso Unitário: </strong>{{ $produto->peso_unitario }}</div>
                <div><strong>NCM: </strong>{{ $produto->ncm }}</div>
                <div><strong>Transporte: </strong>{{ $produto->container }}</div>
                
                @if(isset($produto->qtd_caixa) && !empty($produto->qtd_caixa))
                    <div><strong>Unidades por caixa: </strong>{{ $produto->qtd_caixa }}</div>
                @endif

                @if(isset($produto->cla) && !empty($produto->cla))
                    <div><strong>Dimensões caixa (CLA): </strong>{{ $produto->cla }}</div>
                @endif
                
                @if(isset($produto->m3_caixa) && !empty($produto->m3_caixa))
                    <div><strong>M3 Caixa: </strong>{{ $produto->m3_caixa }}</div>
                @endif

                @if(isset($produto->peso_caixa) && !empty($produto->peso_caixa))
                    <div><strong>Peso bruto com a caixa: </strong>{{ $produto->peso_caixa }}</div>
                @endif

            </div>
        </div>
    </div>
</section>

@endsection
