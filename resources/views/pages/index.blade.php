@extends('layouts.default')

@section('social-tags')

    <meta property="og:title" content="Kinino Export">
    <meta property="og:description" content="{{ $informations->meta_description }}">
    <meta property="og:image" content="{{ asset('img/banners/'.$single_image->image) }}">
    <meta property="og:image:alt" content="{{ asset('img/logo.png') }}">

    <meta property="og:image:width" content="1920"/>
    <meta property="og:image:height" content="820"/>
    <meta property="og:url" content="{{ route('nav.index', ['locale' => App::getLocale()]) }}">

    <meta name="twitter:title" content="Kinino Export">
    <meta name="twitter:description" content="{{ $informations->meta_description }}">
    <meta name="twitter:image"content="{{ asset('img/banners/'.$single_image->image) }}">
    <meta name="twitter:card" content="summary_large_image">

@endsection

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset("plugins/Nivo-Slider-jQuery-master/nivo-slider.css") }}"
    title="bootstrap">
@endsection
@section('scripts')
<script src="{{ asset("plugins/Nivo-Slider-jQuery-master/jquery.nivo.slider.js") }}"></script>
@endsection

@section('content')
<section id="slider">
    <div class="container-fluid">
        <div class="row">

            <div id="banner-slide" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @foreach ($banners as $banner)
                    <div class="carousel-item {{ $loop->index == 0?"active":"" }}">
                        <img class="d-block w-100" src="{{ asset('/img/banners/' . $banner->image) }}"
                            alt="First slide">
                    </div>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
</section>

{{-- <section class="linha">
    <div class="container">
        <div class="row">
            @foreach ($line as $linhas)
            <div class="col-md-4 col-sm-6 col-12">
                <a href="{{ route('subcategories_products',['locale'=>App::getLocale(),'url'=>$linhas->url])}}">
<div class="block-linha line-wrapper position-relative"
    style="background-image: url({{ asset('img/linhas/'.$linhas->img) }});"></div>
</a>
</div>
@endforeach
</div>
</div>
</section> --}}
<a id="itens"></a>
<section class="categories">
    <div class="container">
        <div class="row">

            <div class="col-12 title-index">
                <h3 class="category-index text-center">
                    <span class="cat-title">@lang('site.linha1')</span><br>
                    <span class="cat-subtitle">@lang('site.linha2')</span></span>
                </h3>
            </div>

            {{-- @foreach ($category as $subcategory)
            <div class="col-md-4 col-6">
                <a href="{{ route('subcategories_products',['locale'=>App::getLocale(),'url'=>$subcategory->url])}}">
            <div class="product-wrapper position-relative"
                style="background-image: url({{ asset('img/back-categorias.png') }})">
                <p>{{ $subcategory->nome }}</p>
            </div>
            </a>
        </div>
        @endforeach --}}
        </div>

        <div class="row categories-list">

            @foreach ($categories as $category)
            <div class="col-lg-4 col-md-6 col-12 product-content">
                <a href="{{ route('nav.produtos', ['locale' => App::getLocale(), 'categoria' => $category->url]) }}">
                    <div class="product-wrapper"
                        style="background-image:url('{{ asset('img/categorias/'.$category->image) }}')">
                        <div class="product-wrapper-inner d-flex align-items-center">
                            <div class="container">
                                <div class="row">
                                    <h1>{{ $category->title }}</h1>
                                    <p>{{ $category->short_description }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach

        </div>
    </div>
</section>

<script>
    $('.carousel').carousel({
        interval: 5000,
        pause: "false"
    })
</script>


@endsection
