@extends('layouts.default')

@section('social-tags')

    <meta property="og:title" content="Kinino Export">
    <meta property="og:description" content="{{ (request()->get('s')!="")?"Procurando por ".request()->get('s'):$categoria->title }}">
    <meta property="og:image" content="{{ asset('img/banners/'.$single_image->image) }}">
    <meta property="og:image:alt" content="{{ asset('img/logo.png') }}">

    <meta property="og:image:width" content="1920"/>
    <meta property="og:image:height" content="820"/>
    <meta property="og:url" content="{{ 
        (!empty(request()->get('s')))
            ? route('nav.procurar', ['locale' => App::getLocale(), 's' => request()->get('s')]) 
            : route('nav.produtos', ['locale' => App::getLocale(), 'categoria' => $categoria->url]) 
            }}">
    <meta name="twitter:title" content="Kinino Export">
    <meta name="twitter:description" content="{{ (request()->get('s')!="")?"Procurando por ".request()->get('s'):$categoria->title }}">
    <meta name="twitter:image"content="{{ asset('img/banners/'.$single_image->image) }}">
    <meta name="twitter:card" content="summary_large_image">

@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a
                            href="{{ route('nav.index', ['locale' => App::getLocale()]) }}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">

                        @if (request()->get('s'))
                            <a href="#">Procurando por: "{{ request()->get('s') }}"</a>
                        @else
                            <a href="{{ route('nav.produtos', ['locale' => App::getLocale(), 'categoria' => $categoria->url]) }}">{{ $categoria->title }}</a>
                        @endif

                    </li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 text-center">
            {{ request()->get('s') ? 'Procurando por: ' . request()->get('s') : $categoria->title }}
        </div>
    </div>
</div>
<section class="products">
    <div class="container">
        <div class="row">
            @if ($produtos->count() == 0)
                <div class="col-12 text-center">
                    <div class="no-search-wrapper">
                        <h2>Não encontramos produtos para sua busca</h2>
                        <h2>Talvez sua busca tenha sido específica demais. Pesquise novamente usando termos mais genéricos.</h2>
                    </div>
                    <img src="{{ asset('img/not-found.jpg') }}" alt="">
                </div>
            @else
                @foreach ($produtos as $produto)
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6 product-item">
                        <a href="{{ route('nav.produto', ['locale' => App::getLocale(), 'url' => $produto->url]) }}">
                            <img src="{{ asset('img/produtos/'.$produto->image) }}" alt="{{ $produto->title }}" class="img-fluid">
                        </a>
                        <div class="product-name">{{ $produto->title }}</div>
                        <div><strong>Cód.: </strong>{{ $produto->sku }}</div>
                        <a class="text-link" href="{{ route('nav.produto', ['locale' => App::getLocale(), 'url' => $produto->url]) }}">Saiba Mais</a>
                    </div>
                @endforeach
            @endif
        </div>
        <div class="text-xs-center text-center">
            {{ $produtos->appends(request()->except('page'))->links() }}
        </div>
    </div>
</section>
@endsection
