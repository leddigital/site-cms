<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::redirect('/', '/es');

 Route::group(['prefix' => '{locale}'], function(){

    Route::get('/', 'NavigationController@index')->name('nav.index');
    Route::get('/contato', 'NavigationController@sobre')->name('nav.contato');
    Route::get('/produtos/{categoria}', 'NavigationController@produtos')->name('nav.produtos');
    Route::get('/produto/{url}', 'NavigationController@produto')->name('nav.produto');
    Route::get('/procurar', 'NavigationController@procurar')->name('nav.procurar');

});

// Cadastrar produtos pela planilha
Route::get('/insert/categories', 'InsertCategories@save');
Route::get('/insert/products', 'InsertProducts@save');

//Enviar e-mail
Route::post('/enviar/email', 'EmailController@email')->name('send.mail');

Auth::routes();

//Rotas para utilizacao de autenticação
Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {

    //Informações Gerais
    Route::get('/', 'InformationsController@index')->name('information.index');
    Route::post('/informacoes/salvar', 'InformationsController@save')->name('information.save');

    //Banners
    Route::get('/banners', 'BannersController@index')->name('banners.index');
    Route::get('/banners/listar/todos', 'BannersController@readAll')->name('banners.list.all');
    Route::get('/banners/editar/{id?}', 'BannersController@form')->name('banners.edit');
    Route::get('/banners/cadastro', 'BannersController@form')->name('banners.form');
    Route::post('/banners/salvar', 'BannersController@save')->name('banners.save');
    Route::post('/banners/editar/salvar/{id}', 'BannersController@save')->name('banners.edit.save');
    Route::get('/banners/deletar/{id?}', 'BannersController@delete')->name('banners.delete');

    //Categorias
    Route::get('/categorias/{_type}', 'CategoriesController@index')->name('categories.index');
    Route::get('/categorias/listar/todos/{_type}', 'CategoriesController@readAll')->name('categories.list.all');
    Route::get('/categorias/editar/{_type}/{id?}', 'CategoriesController@form')->name('categories.edit');
    Route::get('/categorias/cadastro/{_type}', 'CategoriesController@form')->name('categories.form');
    Route::post('/categorias/salvar/{_type}', 'CategoriesController@save')->name('categories.save');
    Route::post('/categorias/editar/salvar/{_type}/{id}', 'CategoriesController@save')->name('categories.edit.save');
    Route::get('/categorias/deletar/{id?}', 'CategoriesController@delete')->name('categories.delete');

    //Conteudo - ES
    Route::get('/producto/{_type}', 'ContentsController@index')->name('content.es.index');
    Route::get('/producto/listar/todos/{_type}', 'ContentsController@readAll')->name('content.list.all');
    Route::get('/producto/editar/{_type}/{id?}', 'ContentsController@form')->name('content.edit');
    Route::get('/producto/cadastro/{_type}', 'ContentsController@form')->name('content.form');
    Route::post('/producto/salvar/{_type}', 'ContentsController@save')->name('content.es.save');
    Route::post('/producto/editar/salvar/{id}', 'ContentsController@save')->name('content.edit.save');
    Route::get('/producto/deletar/{id?}', 'ContentsController@delete')->name('content.delete');

    //Conteudo - EN
    Route::get('/product/{_type}', 'ContentsController@index')->name('content.en.index');
    Route::get('/product/listar/todos/{_type}', 'ContentsController@readAll')->name('content.list.all');
    Route::get('/product/editar/{_type}/{id?}', 'ContentsController@form')->name('content.edit');
    Route::get('/product/cadastro/{_type}', 'ContentsController@form')->name('content.form');
    Route::post('/product/salvar/{_type}', 'ContentsController@save')->name('content.en.save');
    Route::post('/product/editar/salvar/{id}', 'ContentsController@save')->name('content.edit.save');
    Route::get('/product/deletar/{id?}', 'ContentsController@delete')->name('content.delete');

});
